from astropy.io import fits
import corner
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
from sys import argv
from joblib import Parallel, delayed
import re
import os
import time


# funcao que ordena as pastas nas pastas das galaxias processadas, pois linux quebra ordem dos Runs
"""
File to create panel graphs in parallel manel
First argument is path to folder containing catalogs/processed
Second is a model name
Third is number of cpu for a job
Fouth is starting name of folder containing Run* folders

example: 
python posteriorPanelCreatorParallel.py /path/to/outputGalphatSersic/ Sersic 3 Run
or
python posteriorPanelCreatorParallel.py /path/to/outputGalphatSersicPlusExp/ Sersic+Exponential 3 Run

in loop form

for i in {start..end}; do python posteriorPanelCreatorParallel.py /path/to/outputGalphatSersicPlusExp/ Sersic+Exponential 3 Run; done
"""

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def custom_sort(t):
    if t[0]=='R':
    	temp = re.search('Run(.*)_58', t) # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
    	return int(temp.group(1))

def getProcessed(path, processedList, startName):
    files = os.listdir(path)
    
    newlist = []
    
    for names in files:
        if names.startswith(startName):
            newlist.append(names)
    newlist.sort(key=natural_keys)  # ordena de Run1 ate RunN-1
    
    for file in newlist:
        id = re.search('_(.*)_band_r', file)
        processedList.append(id.group(1))

    return newlist


def findConvergenceLine(convergnceFile):
    stringToMatch = "Converged at n="

    with open(convergnceFile, 'r') as file:
        for line in file:
            if stringToMatch in line:
                file.close()
                convertgedAt = int(line.split("n=", 1)[1])
                break

    return convertgedAt


def plotPanel(path, targetFolders, processedList, model, step):
    #for folder in targetFolders:
    fitsFile = path+targetFolders[int(step)]+'/new.posterior'+str(step+1)+'_'+processedList[int(step)]+'_band_r.fits.gz'
    pngName = path+targetFolders[int(step)]+'/new.posterior'+str(step+1)+'_'+processedList[int(step)]+'_band_r.png'
    fileConvergedInfo = path+targetFolders[int(step)]+'/new.bie.0.log'
    if not (os.path.isfile(pngName)):
        if os.path.isfile(fitsFile):
            dataRaw = fits.open(fitsFile)
            data = dataRaw[1].data
            convertgedAt = findConvergenceLine(fileConvergedInfo)
            data = data[convertgedAt:]  # cortar tudo antes de convergencia

            if model == 'Sersic+Exponential':
                X = data.field(5)
                Y = data.field(6)
                MAG = data.field(7)
                BT = data.field(8)
                Rhalf = data.field(9)
                DB = data.field(10)
                N = data.field(11)
                Q_disc = data.field(12)
                PA_disc = data.field(13)
                Q_bulge = data.field(14)
                PA_bulge = data.field(15)
                Sky = data.field(16)

                labels = [r"$Sky$", r"$PA_{bulge}$", r"$Q_{bulge}$", r"$PA_{disc}$",
                        r"$Q_{disk}$", r"$DB$", r"$Rhalf$", r"$BT$", r"$MAG$", r"$Y$", r"$X$"]
                samples = np.array((Sky, PA_bulge, Q_bulge, PA_disc,
                                    Q_disc, DB, Rhalf, BT, MAG, Y, X)).T

            elif model == 'Sersic':
                X = data.field(5)
                Y = data.field(6)
                MAG = data.field(7)
                Rhalf = data.field(8)
                N = data.field(9)
                Q_disc = data.field(10)
                PA_disc = data.field(11)
                Sky = data.field(12)

                labels = [r"$Sky$", r"$PA_{disc}$", r"$Q_{disc}$",
                        r"$N$", r"$R_{half}$", r"$Mag$", r"$Y$", r"$X$"]
                samples = np.array((Sky, PA_disc, Q_disc, N, Rhalf, MAG, Y, X)).T

            plt.style.use('default')
            rcParams["font.size"] = 14

            figure = corner.corner(samples, reverse=False, labels=labels,
                                quantiles=[0.16, 0.5, 0.84],
                                show_titles=True, title_kwargs={"fontsize": 14}, label_kwargs={"fontsize": 14})

            fileName = re.sub('.fits.gz', '', fitsFile)
            print(fileName)
            figure.savefig(fileName+'.png')
            plt.close(figure)


path = argv[1] #../test100/processed6/outputGalphatSersic/
model = str(argv[2]) #Sersic or Sersic+Exponential
numCPU = int(argv[3])
startName = str(argv[4])


processedList = []
processedFolders = getProcessed(path, processedList, startName)

Parallel(n_jobs=numCPU)(delayed(plotPanel)(path, processedFolders, processedList, model, step) for step in range(len(processedList)))
