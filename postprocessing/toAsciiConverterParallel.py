import os
import re
from sys import argv
from astropy.table import Table
from astropy.io import fits
from joblib import Parallel, delayed
import glob

""" 
Converts all ascii files in first argument to fits, ssecond argument 0 for leave ascii, 1 to delete it 
example
python toAsciiConverterParallel.py ./path/to/outputGalphatSersicPlusExp/ 0 2
or
python toAsciiConverterParallel.py ./path/to/outputGalphatSersic/ 0 2
in form of a for loop
for i in {start..end}; do python toAsciiConverterParallel.py ../NewPrior/processed$i/outputGalphatSersicPlusExp/ 0 2; done
"""
# funcao que ordena as pastas nas pastas das galaxias processadas, pois linux quebra ordem dos Runs


def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def custom_sort(t):
    if t[0] == 'R':
        # ordena usando digitos que vem depois de Run e antes de _58, que eh o numero de Run
        temp = re.search('Run(.*)_58', t)
        return int(temp.group(1))

def getProcessed(path, processedList, startsWith):
        files = os.listdir(path)
        #files.sort(key=custom_sort) # ordena de Run1 ate RunN-1
        files.sort(key=natural_keys)
        filesFilteres = []
        for file in files:
            #if file[0] == 'R': # seleciona todas pastas que comecam com R maiusculo, ignora resto
            if file.startswith(startsWith):
                id = re.search('_(.*)_band_r', file) 
                processedList.append(id.group(1))
                filesFilteres.append(file)
        
        return filesFilteres

def convertToFits(path, processedList, processedFolders, flag, step):
    #for i in range(len(processedFolders)):
        postiriorPath = path + processedFolders[step] + '/new.posterior' + str(step+1) + '_' + processedList[step] + '_band_r'
        print(postiriorPath)
        #if not (os.path.isfile(postiriorPath)):
        if os.path.isfile(postiriorPath):
            print(postiriorPath)
            table = Table.read(postiriorPath, format='ascii')
            table.write(postiriorPath+".fits.gz",overwrite=True)
        if (os.path.isfile(postiriorPath)):
            if flag==1:
                os.remove(postiriorPath)
        
        #look if there are any bie chainlogs
        if os.path.isfile(path + processedFolders[step] + '/bie.chainlog.0'):
            #remove chainlog
            fileList = glob.glob(path + processedFolders[step] + '/bie.chainlog.*')
            for filePath in fileList:
                try:
                    os.remove(filePath)
                except:
                    print("Error while deleting file : ", filePath)


path = argv[1]
deleteORleave = int(argv[2])
numCPU = int(argv[3])

processedList = []
processedFolders = getProcessed(path, processedList, 'Run')
#convertToFits(path, processedList, processedFolders, deleteORleave)

Parallel(n_jobs=numCPU)(delayed(convertToFits)(path, processedList, processedFolders, deleteORleave, step) for step in range(len(processedList)))
