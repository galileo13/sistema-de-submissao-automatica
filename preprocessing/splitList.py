from sys import argv
import sys
import os

galaxiesPerList = int(argv[1])
inputFilePath = str(argv[2])
inputFileName = str(argv[3])
numberLists = str(argv[4])
print("Output from Python file")
print("Big list was found in "+argv[2]+", it name is "+argv[3])
print("Creating " + numberLists + " lists with " + argv[1] + " galaxies each")

csvfile = open(inputFilePath+inputFileName, 'r').readlines()
filename = 1
for i in range(len(csvfile)):
    if i % galaxiesPerList == 0:
        file = open(inputFilePath+ str(filename) + '.csv', 'w+')
        file.writelines("objid\n")
        file.writelines(csvfile[i:i+galaxiesPerList])
        filename += 1

print("End of output from python file")