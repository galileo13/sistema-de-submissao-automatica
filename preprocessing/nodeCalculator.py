from sys import argv
import sys
import os
import math

numberCores = int(argv[1])
threadsPerNode = int(argv[2])


if(numberCores%threadsPerNode == 0):
    numberNodes = numberCores/threadsPerNode
else:
    numberNodes = math.ceil(numberCores/threadsPerNode)

sys.exit(int(numberNodes))

