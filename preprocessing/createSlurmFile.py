from sys import argv
import sys
import os

inputFilePath = str(argv[1])
inputFileName = str(argv[2])
numberInstances = int(argv[3])
numberNodes = str(argv[4])
numberCores = str(argv[5])
queueName = str(argv[6])
jobTime = int(argv[7])
modelName = str(argv[8])
fullPath = str(argv[9])
scratchPath = str(argv[10])
machine = str(argv[11])

# formatting time parametr to get hh:mm:ss
if jobTime < 10:
    jobTime='0'+str(jobTime)

def createSlurmFile(inputFilePath, inputFileName, numberInstances, numberNodes, numberCores, queueName, jobTime, modelName):
    line = '#!/bin/bash\n'
    line = line + '#SBATCH --nodes='+numberNodes+'\n'
    line = line + '#SBATCH --ntasks-per-node=24\n'
    line = line + '#SBATCH --ntasks='+numberCores+'\n'
    line = line + '#SBATCH -p '+queueName+'\n'
    line = line + '#SBATCH -J pname\n'
    line = line + '#SBATCH --time=' + str(jobTime) + ':00:00' + '\n' 
    line = line + '#SBATCH --exclusive\n\n'

    line = line + '# Exibe os no alocados para o Job\n'
    line = line + 'echo $SLURM_JOB_NODELIST\n'
    line = line + 'nodeset -e $SLURM_JOB_NODELIST\n'
    line = line + 'cd $SLURM_SUBMIT_DIR\n\n'
    
    line = line + '# Configures compilers for the compiler suite\n'
    line = line + 'source /scratch/app/modulos/intel-psxe-2018.sh\n'
    line = line + 'module load bie/current_openmpi+intel\n\n'
    
    for i in range(numberInstances):
        line = line + 'csh ' + fullPath + '/preprocessing/scriptsRunAndCheck/submitJobsMain ' + scratchPath + '/catalog'+str(i+1)+'/listComplete '+modelName+' '+machine' &\n'
    
    line = line + 'wait\n'
    filePtr=open(inputFilePath+inputFileName,'w+')
    filePtr.write(line)
    filePtr.close()

createSlurmFile(inputFilePath, inputFileName, numberInstances, numberNodes, numberCores, queueName, jobTime, modelName)