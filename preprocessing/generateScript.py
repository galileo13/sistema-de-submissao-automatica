from astropy.io import fits as pyfits
import numpy as np

def definePriors(outDirFits,galaxy):
    header = pyfits.getheader(outDirFits+galaxy.imgfile,0)

    galaxy.gain=float(header['gain'])
    galaxy.rdnoise=float(header['rdnoise'])
    galaxy.zeroPoint=float(header['zP'])
    
    galaxy.SizeX=int(header['SizeX'])
    galaxy.SizeY=int(header['SizeY'])
    galaxy.xc=float(header['XC'])
    galaxy.yc=float(header['YC'])
  
    galaxy.priorX='NormalDist(0.0,1.5,-3.0, 3.0)'
    galaxy.priorCtrlX='Additive'
    galaxy.priorOffX=str(galaxy.SizeX/2)#see stamp creation xc
    galaxy.priorInitX=str(galaxy.xc-galaxy.SizeX/2)

    galaxy.priorY='NormalDist(0.0,1.5,-3.0, 3.0)'
    galaxy.priorCtrlY='Additive'
    galaxy.priorOffY=str(galaxy.SizeY/2)#see stamp creation yc
    galaxy.priorInitY=str(galaxy.yc-galaxy.SizeY/2)
 
    galaxy.priorMag='NormalDist('+str(galaxy.Mag)+', 0.2, '+str(galaxy.Mag-1.0)+','+str(galaxy.Mag+1.0)+')'
    galaxy.priorCtrlMag='None'
    galaxy.priorOffMag='0.0'
    galaxy.priorInitMag=str(galaxy.Mag)
    galaxy.priorRe='WeibullDist(1.21, 2.5, 0.1, 10.0)'
    galaxy.priorCtrlRe='Scaled'
    galaxy.priorOffRe=str(galaxy.re_pix)
    galaxy.priorInitRe=str(1.0)
    
    galaxy.priorN='NormalDist(6.0, 6.0, 0.5000001, 13.99999)'
    galaxy.priorCtrlN='None'
    galaxy.priorOffN='0.0'
    galaxy.priorInitN=str(6.0)

    galaxy.priorQ='NormalDist('+str(galaxy.q)+',0.3,0.09, 0.99999)'
    galaxy.priorCtrlQ='None'
    galaxy.priorOffQ='0.0'
    galaxy.priorInitQ=str(galaxy.q)
    
    galaxy.priorPA='NormalDist(0,0.69, -1.57, 1.57)'
    galaxy.priorCtrlPA='Additive'

    PA=float(header['PAoff'])
    if PA < 0.0:
       PA=(np.pi+PA)

    galaxy.priorOffPA=str(PA)#see stamp creation
    galaxy.priorInitPA=str(0.0)
    
    galaxy.priorPS_MAG='NormalDist('+str(galaxy.Mag+3.0)+', 5.0)'
    galaxy.priorCtrlPS_MAG='None'
    galaxy.priorInitPS_MAG=str(0.0)
    
    galaxy.priorBT='UniformDist(0.01, 0.8)'
    galaxy.priorCtrlBT='None'
    galaxy.priorInitBT=str(0.4)

    galaxy.priorDB='UniformDist(1.0, 15)'
    galaxy.priorCtrlDB='None'
    galaxy.priorInitDB=str(4.0)

    galaxy.priorQb='WeibullDist(1.0, 2.5, 0.2, 0.9999)'
    galaxy.priorCtrlQb='None'
    galaxy.priorInitQb=str(galaxy.q)
    
    galaxy.priorQd='UniformDist(0.09, 0.99)'
    galaxy.priorCtrlQd='None'
    galaxy.priorInitQd=str(0.5)

    galaxy.priorPAb='NormalDist(0,0.69, -0.79, 0.79)'
    galaxy.priorCtrlPAb='Additive'
    galaxy.priorOffPAb=PA#see stamp creation
    galaxy.priorInitPAb=str(0.0)
    
    galaxy.priorPAd='NormalDist(0,0.69, -0.79, 0.79)'
    galaxy.priorCtrlPAd='Offset from bulge'
    galaxy.priorInitPAd=str(0.1)

    galaxy.priorSky='NormalDist(1,0.01,0.97,1.03)'  
    galaxy.priorCtrlSky='Multiplicative'
    galaxy.priorOffSky=str(float(header['SkyOff'])) #see stamp creation     
    galaxy.priorInitSky=str(1.0)
    
    return galaxy

def inputGalphatBMD(outDir,galaxy,inputfitspath,filename):
    linha='{\n    "parameters":\n    {\n\t"_comment": "A simple test for a one-dimensional profile",\n'
    linha=linha+'\t"_comment": "I use _comment for comments by convention",\n'
    linha=linha+'\t"_comment": ["In JSON, all strings must be surrounded by",\n'
    linha=linha+'\t\t     "double quotes, and name tags are strings.",\n'
    linha=linha+'\t\t     "String values must be surrount in double quotes",\n'
    linha=linha+'\t\t     "but integer or float types do not need quotes."],\n\t"author": "Diego Stalder, Igor Kolesnikov",\n'
    linha=linha+'\t"date":\n\t{\n\t    "day": 29,\n\t    "month": "September",\n\t    "year": 2015\n\t},\n'
    linha=linha+'\t"control":\n\t{\n\t    "1": {\n\t\t"description": ["GALPHAT control parameters for Level 1.",\n'
    linha=linha+'\t\t\t\t"\\"decimate=1\\" turns off pixel aggregation.",\n\t\t\t\t"sshape is the 2MASS seeing shape parameter",\n'
    linha=linha+'\t\t\t\t"to determine PSF FWHM and not used if the",\n\t\t\t\t"PSF image is specified."],\n'
    linha=linha+'\t\t"level":\n\t\t{\n\t\t    "desc": "data aggregation level",\n\t\t    "value": 1\n\t\t},\n'
    linha=linha+'\t\t"image":\n\t\t{\n\t\t    "desc": "Input data, FITS file",\n\t\t    "value": "'+inputfitspath+galaxy.imgfile+'"\n\t\t},\n'
    linha=linha+'\t\t"psf":\n\t\t{\n\t\t    "desc": "PSF image, FITS file",\n\t\t    "value": "'+inputfitspath+galaxy.psffile+'"\n\t\t},\n'
    linha=linha+'\t\t"mask":\n\t\t{\n\t\t    "desc": "Bad pixel mask",\n\t\t    "value": "'+inputfitspath+galaxy.maskfile+'"\n\t\t},\n'
    linha=linha+'\t\t"region":\n\t\t{\n\t\t    "desc": "Image reagion to fit",\n\t\t    "value": "1 '+str(galaxy.SizeX)+' 1 '+str(galaxy.SizeY)+' "\n\t\t},\n'
    linha=linha+'\t\t"zeropt":\n\t\t{\n\t\t    "desc": "Photometric zero point",\n\t\t    "value": '+str(galaxy.zeroPoint)+'\n\t\t},\n'
    linha=linha+'\t\t"adjzpt":\n\t\t{\n\t\t    "desc": "Adjust zero pt for exposure",\n\t\t    "value": false\n\t\t},\n'
    linha=linha+'\t\t"aggregate":\n\t\t{\n\t\t    "desc": "Image aggregration factor",\n\t\t    "value": 1 \n\t\t},\n'
    linha=linha+'\t\t"shape":\n\t\t{\n\t\t    "desc": "seeing shape",\n\t\t    "value": 1.100\n\t\t},\n'
    linha=linha+'\t\t"gain":\n\t\t{\n\t\t    "desc": "gain, e\/ADU",\n\t\t    "value": '+str(galaxy.gain)+'\n\t\t},\n'
    linha=linha+'\t\t"rdnoise":\n\t\t{\n\t\t    "desc": "ReaoutNoise of the image in electrons",\n\t\t    "value": '+str(galaxy.rdnoise)+'\n\t\t},\n'
    linha=linha+'\t\t"coadd":\n\t\t{\n\t\t    "desc": "Number of coadded images",\n\t\t    "value": 1.0\n\t\t},\n'
    linha=linha+'\t\t"xoffset":\n\t\t{\n\t\t    "desc": "origin offset in x direction",\n\t\t    "value": '+galaxy.priorOffX+'\n\t\t},\n'
    linha=linha+'\t\t"yoffset":\n\t\t{\n\t\t    "desc": "origin offset in y direction",\n\t\t    "value": '+galaxy.priorOffY+'\n\t\t},\n'
    linha=linha+'\t\t"magi":\n\t\t{\n\t\t    "desc": "fiducial magitude (i.e. estimated from catalog)",\n\t\t    "value": '+galaxy.priorOffMag+' \n\t\t},\n'
    linha=linha+'\t\t"rscale":\n\t\t{\n\t\t    "desc": "r scale",\n\t\t    "value": '+galaxy.priorOffRe+'\n\t\t},\n'
    linha=linha+'\t\t"noffset":\n\t\t{\n\t\t    "desc": "offset in n",\n\t\t    "value": '+galaxy.priorOffN+'\n\t\t},\n'
    linha=linha+'\t\t"qoffset":\n\t\t{\n\t\t    "desc": "offset in axis ratio",\n\t\t    "value":  '+galaxy.priorOffQ+'\n\t\t},\n'
    linha=linha+'\t\t"paoffset":\n\t\t{\n\t\t    "desc": "offset in position angle",\n\t\t    "value":  '+galaxy.priorOffPA+'\n\t\t},\n'
    linha=linha+'\t\t"bgoffset":\n\t\t{\n\t\t    "desc": "offset in background",\n\t\t    "value":  '+galaxy.priorOffSky+'\n\t\t}\n\t  \t }\n\t  },\n'
    linha=linha+'\t"models":\n\t{\n\t    "description": ["Initial guess for GALPHAT: id = id counter,",\n'
    linha=linha+'\t\t\t    "name = parameter name to print out,",\n'
    linha=linha+'\t\t\t    "desc = parameter description,",\n\t\t\t    "ctrl = parameter transform type",\n'
    linha=linha+'\t\t\t    "(i.e. Additive, Multiplicative, Scaled, etc.)"],\n'
    linha=linha+'\t    "_comment": ["The \\"model type\\" is a required entity. All other",\n'
    linha=linha+'\t\t\t "entities will be registered and their tags used as",\n\t\t\t "string labels. "],\n'
    
    linhaBMD=linha+'\t    "BulgeDisk":\n\t    {\n'
    linhaNormal=linha+'\t    "Single":\n\t    {\n'
    
    linha='\t\t"X center":\n\t\t{\n\t\t    "desc": "X center rel to lower corner",\n'
    linha=linha+'\t\t    "index": 0,\n\t\t    "ctrl": "'+galaxy.priorCtrlX+'",\n\t\t    "value": '+galaxy.priorInitX+'\n\t\t},\n'
    linha=linha+'\t\t"Y center":\n\t\t{\n\t\t    "desc": "Y center rel to lower corner",\n'
    linha=linha+'\t\t    "index": 1,\n\t\t    "ctrl": "'+galaxy.priorCtrlY+'",\n\t\t    "value": '+galaxy.priorInitY+'\n\t\t},\n'
    linha=linha+'\t\t"Mag":\n\t\t{\n\t\t    "desc": "total magnitude relative to fiducial value, magi",\n'
    linha=linha+'\t\t    "index": 2,\n\t\t    "ctrl": "'+galaxy.priorCtrlMag+'",\n\t\t    "value": '+galaxy.priorInitMag+'\n\t\t},\n'
    
    linhaNormal=linhaNormal+linha
    linhaBMD=linhaBMD+linha
    
    linha='\t\t"R_e":\n\t\t{\n\t\t    "desc": "Effective radius scaled by the fiducial radius, rscale",\n'
    linha=linha+'\t\t    "index": 3,\n\t\t    "ctrl": "'+galaxy.priorCtrlRe+'",\n\t\t    "value": '+galaxy.priorInitRe+'\n\t\t},\n'
    linha=linha+'\t\t"Sersic":\n\t\t{\n\t\t    "label": "Sersic n",\n\t\t    "desc": "Sersic index",\n'
    linha=linha+'\t\t    "index": 4,\n\t\t    "ctrl": "'+galaxy.priorCtrlN+'",\n\t\t    "value": '+galaxy.priorInitN+'\n\t\t},\n'
    linha=linha+'\t\t"q":\n\t\t{\n\t\t    "desc": "axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 5,\n\t\t    "ctrl": "'+galaxy.priorCtrlQ+'",\n\t\t    "value": '+galaxy.priorInitQ+'\n\t\t},\n'
    linha=linha+'\t\t"PA":\n\t\t{\n\t\t    "desc": "position angle in radians rel to PA offset",\n'
    linha=linha+'\t\t    "index": 6,\n\t\t    "ctrl": "'+galaxy.priorCtrlPA+'",\n\t\t    "value": '+galaxy.priorInitPA+'\n\t\t},\n'
   
    linhaNormal=linhaNormal+linha
    
    linha='\t\t"B/T":\n\t\t{\n\t\t    "desc": "Bulge-to-total flux ratio",\n'
    linha=linha+'\t\t    "index": 3,\n\t\t    "ctrl": "'+galaxy.priorCtrlBT+'",\n\t\t    "value": '+galaxy.priorInitBT+'\n\t\t},\n'
    linha=linha+'\t\t"R_half":\n\t\t{\n\t\t "desc": "Half-light radius scaled by the fiducial radius, rscale",\n'
    linha=linha+'\t\t    "index": 4,\n\t\t    "ctrl": "'+galaxy.priorCtrlRe+'",\n\t\t    "value": '+galaxy.priorInitRe+'\n\t\t},\n'
    linha=linha+'\t\t"D/B":\n\t\t{\n\t\t    "desc": "ratio of disk to bulge size",\n'
    linha=linha+'\t\t    "index": 5,\n\t\t    "ctrl": "'+galaxy.priorCtrlDB+'",\n\t\t    "value": '+galaxy.priorInitDB+'\n\t\t},\n'
    linha=linha+'\t\t"Sersic":\n\t\t{\n\t\t  "desc": "bulge Sersic index",\n'
    linha=linha+'\t\t    "index": 6,\n\t\t    "ctrl": "'+galaxy.priorCtrlN+'",\n\t\t    "value": '+galaxy.priorInitN+'\n\t\t},\n' 
    linha=linha+'\t\t"q_bulge":\n\t\t{\n\t\t    "desc": "bulge axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlQb+'",\n\t\t    "value": '+galaxy.priorInitQb+'\n\t\t},\n'
    linha=linha+'\t\t"q_disk":\n\t\t{\n\t\t    "desc": "disk axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 8,\n\t\t    "ctrl": "'+galaxy.priorCtrlQd+'",\n\t\t    "value": '+galaxy.priorInitQd+'\n\t\t},\n'
    linha=linha+'\t\t"PA_bulge":\n\t\t{\n\t\t    "desc": "position angle in radians relative to pa offset",\n'
    linha=linha+'\t\t    "index": 9,\n\t\t    "ctrl": "'+galaxy.priorCtrlPAb+'",\n\t\t    "value": '+galaxy.priorInitPAb+'\n\t\t},\n'
    linha=linha+'\t\t"PA_disk":\n\t\t{\n\t\t    "desc": "position angle tied to bulge position angle",\n'
    linha=linha+'\t\t    "index": 10,\n\t\t    "ctrl": "'+galaxy.priorCtrlPAd+'",\n\t\t    "value": '+galaxy.priorInitPAd+'\n\t\t},\n'
    
    linha=linha+'\t\t"tables":\n\t\t{\n\t\t    "1":\n'
    linha=linha+'\t\t    {\n\t\t\t"description": ["CAUTION: for Sersic table, fine table",\n'
    linha=linha+'\t\t\t\t\t"(smaller maximum size) should be first.",\n\t\t\t\t\t"Otherwise, you will use coarse resolution",\n'
    linha=linha+'\t\t\t\t\t"table for generating model. Values should",\n\t\t\t\t\t"not be changed with caution."],\n'
    linha=linha+'\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n'
    
    linha=linha+'\t\t\t    "desc": "Model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube cache file",\n\t\t\t    "value": "../../../p800n240r1n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": "800"\n\t\t\t},'
    linha=linha+'\n\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 240\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 1.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'
    linha=linha+'\t\t    "2":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p2000n120r10n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 2000\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 10.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'

    linha=linha+'\t\t    "3":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p1500n120r100n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 1500\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 100.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    
    linha=linha+'\t\t    }\n\t\t}\n\t    },'

    linha=linha+'\n\t    "Sky":\n\t    {\n'
    linha=linha+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linha=linha+'\t\t    "index": 11,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t}\n\t    }\n\t}\n    }\n}'

    linhaBMD=linhaBMD+linha

    linha='\t\t"tables":\n\t\t{\n\t\t    "1":\n'
    linha=linha+'\t\t    {\n\t\t\t"description": ["CAUTION: for Sersic table, fine table",\n'
    linha=linha+'\t\t\t\t\t"(smaller maximum size) should be first.",\n\t\t\t\t\t"Otherwise, you will use coarse resolution",\n'
    linha=linha+'\t\t\t\t\t"table for generating model. Values should",\n\t\t\t\t\t"not be changed with caution."],\n'
    linha=linha+'\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n'
    
    linha=linha+'\t\t\t    "desc": "Model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube cache file",\n\t\t\t    "value": "../../../p800n240r1n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": "800"\n\t\t\t},'
    linha=linha+'\n\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 240\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 1.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'
    linha=linha+'\t\t    "2":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p2000n120r10n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 2000\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 10.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'

    linha=linha+'\t\t    "3":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p1500n120r100n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 1500\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 100.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    
    linha=linha+'\t\t    }\n\t\t}\n\t    },'
  
    linhaPS=linhaNormal+linha+'\n\t    "Point":\n\t    {\n'
    linhaPS=linhaPS+'\t\t"X center":\n\t\t{\n\t\t    "desc": "X center rel to lower corner",\n'
    linhaPS=linhaPS+'\t\t    "index": 0,\n\t\t    "ctrl": "'+galaxy.priorCtrlX+'",\n\t\t    "value": '+galaxy.priorInitX+'\n\t\t},\n'
    linhaPS=linhaPS+'\t\t"Y center":\n\t\t{\n\t\t    "desc": "Y center rel to lower corner",\n'
    linhaPS=linhaPS+'\t\t    "index": 1,\n\t\t    "ctrl": "'+galaxy.priorCtrlY+'",\n\t\t    "value": '+galaxy.priorInitY+'\n\t\t},\n'
    linhaPS=linhaPS+'\t\t"PS mag":\n\t\t{\n\t\t    "desc": "point-source magnitude",\n'
    linhaPS=linhaPS+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlPS_MAG+'",\n\t\t    "value": '+galaxy.priorInitPS_MAG+'\n\t\t}\n\t    },\n'
    linhaPS=linhaPS+'\t    "Sky":\n\t    {\n'
    linhaPS=linhaPS+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linhaPS=linhaPS+'\t\t    "index": 8,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t}\n\t    }\n\t}\n    }\n}'

    linhaNormal=linhaNormal+linha+'\n\t    "Sky":\n\t    {\n'
    linhaNormal=linhaNormal+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linhaNormal=linhaNormal+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t}\n\t    }\n\t}\n    }\n}'
    #BMD
    #linhaBMD=linhaBMD+linhaNormal
    
    filePtr=open(outDir+filename,'w')
    filePtr.write(linhaNormal)
    filePtr.close()

    filePtr=open(outDir+filename+'PS','w')
    filePtr.write(linhaPS)
    filePtr.close()
    
    filePtr=open(outDir+filename+'BMD','w')
    filePtr.write(linhaBMD)
    filePtr.close()

def inputGalphat(outDir,galaxy,inputfitspath,filename):
    linha='{\n    "parameters":\n    {\n\t"_comment": "A simple test for a one-dimensional profile",\n'
    linha=linha+'\t"_comment": "I use _comment for comments by convention",\n'
    linha=linha+'\t"_comment": ["In JSON, all strings must be surrounded by",\n'
    linha=linha+'\t\t     "double quotes, and name tags are strings.",\n'
    linha=linha+'\t\t     "String values must be surrount in double quotes",\n'
    linha=linha+'\t\t     "but integer or float types do not need quotes."],\n\t"author": "Diego Stalder",\n'
    linha=linha+'\t"date":\n\t{\n\t    "day": 29,\n\t    "month": "September",\n\t    "year": 2015\n\t},\n'
    linha=linha+'\t"control":\n\t{\n\t    "1": {\n\t\t"description": ["GALPHAT control parameters for Level 1.",\n'
    linha=linha+'\t\t\t\t"\\"decimate=1\\" turns off pixel aggregation.",\n\t\t\t\t"sshape is the 2MASS seeing shape parameter",\n'
    linha=linha+'\t\t\t\t"to determine PSF FWHM and not used if the",\n\t\t\t\t"PSF image is specified."],\n'
    linha=linha+'\t\t"level":\n\t\t{\n\t\t    "desc": "data aggregation level",\n\t\t    "value": 1\n\t\t},\n'
    linha=linha+'\t\t"image":\n\t\t{\n\t\t    "desc": "Input data, FITS file",\n\t\t    "value": "'+inputfitspath+galaxy.imgfile+'"\n\t\t},\n'
    linha=linha+'\t\t"psf":\n\t\t{\n\t\t    "desc": "PSF image, FITS file",\n\t\t    "value": "'+inputfitspath+galaxy.psffile+'"\n\t\t},\n'
    linha=linha+'\t\t"mask":\n\t\t{\n\t\t    "desc": "Bad pixel mask",\n\t\t    "value": "'+inputfitspath+galaxy.maskfile+'"\n\t\t},\n'
    linha=linha+'\t\t"region":\n\t\t{\n\t\t    "desc": "Image reagion to fit",\n\t\t    "value": "1 '+str(galaxy.SizeX)+' 1 '+str(galaxy.SizeY)+' "\n\t\t},\n'
    linha=linha+'\t\t"zeropt":\n\t\t{\n\t\t    "desc": "Photometric zero point",\n\t\t    "value": '+str(galaxy.zeroPoint)+'\n\t\t},\n'
    linha=linha+'\t\t"adjzpt":\n\t\t{\n\t\t    "desc": "Adjust zero pt for exposure",\n\t\t    "value": false\n\t\t},\n'
    linha=linha+'\t\t"aggregate":\n\t\t{\n\t\t    "desc": "Image aggregration factor",\n\t\t    "value": 1 \n\t\t},\n'
    linha=linha+'\t\t"shape":\n\t\t{\n\t\t    "desc": "seeing shape",\n\t\t    "value": 1.100\n\t\t},\n'
    linha=linha+'\t\t"gain":\n\t\t{\n\t\t    "desc": "gain, e\/ADU",\n\t\t    "value": '+str(galaxy.gain)+'\n\t\t},\n'
    linha=linha+'\t\t"rdnoise":\n\t\t{\n\t\t    "desc": "ReaoutNoise of the image in electrons",\n\t\t    "value": '+str(galaxy.rdnoise)+'\n\t\t},\n'
    linha=linha+'\t\t"coadd":\n\t\t{\n\t\t    "desc": "Number of coadded images",\n\t\t    "value": 1.0\n\t\t},\n'
    linha=linha+'\t\t"xoffset":\n\t\t{\n\t\t    "desc": "origin offset in x direction",\n\t\t    "value": '+galaxy.priorOffX+'\n\t\t},\n'
    linha=linha+'\t\t"yoffset":\n\t\t{\n\t\t    "desc": "origin offset in y direction",\n\t\t    "value": '+galaxy.priorOffY+'\n\t\t},\n'
    linha=linha+'\t\t"magi":\n\t\t{\n\t\t    "desc": "fiducial magitude (i.e. estimated from catalog)",\n\t\t    "value": '+galaxy.priorOffMag+' \n\t\t},\n'
    linha=linha+'\t\t"rscale":\n\t\t{\n\t\t    "desc": "r scale",\n\t\t    "value": '+galaxy.priorOffRe+'\n\t\t},\n'
    linha=linha+'\t\t"noffset":\n\t\t{\n\t\t    "desc": "offset in n",\n\t\t    "value": '+galaxy.priorOffN+'\n\t\t},\n'
    linha=linha+'\t\t"qoffset":\n\t\t{\n\t\t    "desc": "offset in axis ratio",\n\t\t    "value":  '+galaxy.priorOffQ+'\n\t\t},\n'
    linha=linha+'\t\t"paoffset":\n\t\t{\n\t\t    "desc": "offset in position angle",\n\t\t    "value":  '+galaxy.priorOffPA+'\n\t\t},\n'
    linha=linha+'\t\t"bgoffset":\n\t\t{\n\t\t    "desc": "offset in background",\n\t\t    "value":  '+galaxy.priorOffSky+'\n\t\t}\n\t  \t }\n\t  },\n'
    linha=linha+'\t"models":\n\t{\n\t    "description": ["Initial guess for GALPHAT: id = id counter,",\n'
    linha=linha+'\t\t\t    "name = parameter name to print out,",\n'
    linha=linha+'\t\t\t    "desc = parameter description,",\n\t\t\t    "ctrl = parameter transform type",\n'
    linha=linha+'\t\t\t    "(i.e. Additive, Multiplicative, Scaled, etc.)"],\n'
    linha=linha+'\t    "_comment": ["The \\"model type\\" is a required entity. All other",\n'
    linha=linha+'\t\t\t "entities will be registered and their tags used as",\n\t\t\t "string labels. "],\n'
    
    linhaBMD=linha+'\t    "BulgeDisk":\n\t    {\n'
    linhaNormal=linha+'\t    "Single":\n\t    {\n'
    
    linha='\t\t"X center":\n\t\t{\n\t\t    "desc": "X center rel to lower corner",\n'
    linha=linha+'\t\t    "index": 0,\n\t\t    "ctrl": "'+galaxy.priorCtrlX+'",\n\t\t    "value": '+galaxy.priorInitX+'\n\t\t},\n'
    linha=linha+'\t\t"Y center":\n\t\t{\n\t\t    "desc": "Y center rel to lower corner",\n'
    linha=linha+'\t\t    "index": 1,\n\t\t    "ctrl": "'+galaxy.priorCtrlY+'",\n\t\t    "value": '+galaxy.priorInitY+'\n\t\t},\n'
    linha=linha+'\t\t"Mag":\n\t\t{\n\t\t    "desc": "total magnitude relative to fiducial value, magi",\n'
    linha=linha+'\t\t    "index": 2,\n\t\t    "ctrl": "'+galaxy.priorCtrlMag+'",\n\t\t    "value": '+galaxy.priorInitMag+'\n\t\t},\n'
    
    linhaNormal=linhaNormal+linha
    linhaBMD=linhaBMD+linha
    
    linha='\t\t"R_e":\n\t\t{\n\t\t    "desc": "Effective radius scaled by the fiducial radius, rscale",\n'
    linha=linha+'\t\t    "index": 3,\n\t\t    "ctrl": "'+galaxy.priorCtrlRe+'",\n\t\t    "value": '+galaxy.priorInitRe+'\n\t\t},\n'
    linha=linha+'\t\t"Sersic":\n\t\t{\n\t\t    "label": "Sersic n",\n\t\t    "desc": "Sersic index",\n'
    linha=linha+'\t\t    "index": 4,\n\t\t    "ctrl": "'+galaxy.priorCtrlN+'",\n\t\t    "value": '+galaxy.priorInitN+'\n\t\t},\n'
    linha=linha+'\t\t"q":\n\t\t{\n\t\t    "desc": "axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 5,\n\t\t    "ctrl": "'+galaxy.priorCtrlQ+'",\n\t\t    "value": '+galaxy.priorInitQ+'\n\t\t},\n'
    linha=linha+'\t\t"PA":\n\t\t{\n\t\t    "desc": "position angle in radians rel to PA offset",\n'
    linha=linha+'\t\t    "index": 6,\n\t\t    "ctrl": "'+galaxy.priorCtrlPA+'",\n\t\t    "value": '+galaxy.priorInitPA+'\n\t\t},\n'
   
    linhaNormal=linhaNormal+linha
    
    linha='\t\t"B/T":\n\t\t{\n\t\t    "desc": "Bulge-to-total flux ratio",\n'
    linha=linha+'\t\t    "index": 3,\n\t\t    "ctrl": "'+galaxy.priorCtrlBT+'",\n\t\t    "value": '+galaxy.priorInitBT+'\n\t\t},\n'
    linha=linha+'\t\t"R_half":\n\t\t{\n\t\t "desc": "Half-light radius scaled by the fiducial radius, rscale",\n'
    linha=linha+'\t\t    "index": 4,\n\t\t    "ctrl": "'+galaxy.priorCtrlRe+'",\n\t\t    "value": '+galaxy.priorInitRe+'\n\t\t},\n'
    linha=linha+'\t\t"D/T":\n\t\t{\n\t\t    "desc": "ratio of disk to bulge size",\n'
    linha=linha+'\t\t    "index": 5,\n\t\t    "ctrl": "'+galaxy.priorCtrlDB+'",\n\t\t    "value": '+galaxy.priorInitDB+'\n\t\t},\n'
    linha=linha+'\t\t"Sersic":\n\t\t{\n\t\t  "desc": "bulge Sersic index",\n'
    linha=linha+'\t\t    "index": 6,\n\t\t    "ctrl": "'+galaxy.priorCtrlN+'",\n\t\t    "value": '+galaxy.priorInitN+'\n\t\t},\n' 
    linha=linha+'\t\t"q_bulge":\n\t\t{\n\t\t    "desc": "bulge axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlQb+'",\n\t\t    "value": '+galaxy.priorInitQb+'\n\t\t},\n'
    linha=linha+'\t\t"q_disk":\n\t\t{\n\t\t    "desc": "disk axis ratio b\/a",\n'
    linha=linha+'\t\t    "index": 8,\n\t\t    "ctrl": "'+galaxy.priorCtrlQd+'",\n\t\t    "value": '+galaxy.priorInitQd+'\n\t\t},\n'
    linha=linha+'\t\t"PA_bulge":\n\t\t{\n\t\t    "desc": "position angle in radians relative to pa offset",\n'
    linha=linha+'\t\t    "index": 9,\n\t\t    "ctrl": "'+galaxy.priorCtrlPAb+'",\n\t\t    "value": '+galaxy.priorInitPAb+'\n\t\t},\n'
    linha=linha+'\t\t"PA_disk":\n\t\t{\n\t\t    "desc": "position angle tied to bulge position angle",\n'
    linha=linha+'\t\t    "index": 10,\n\t\t    "ctrl": "'+galaxy.priorCtrlPAd+'",\n\t\t    "value": '+galaxy.priorInitPAd+'\n\t\t},\n'
    linha=linha+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linha=linha+'\t\t    "index": 11,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t},\n'

    linhaBMD=linhaBMD+linha

    linha='\t\t"tables":\n\t\t{\n\t\t    "1":\n'
    linha=linha+'\t\t    {\n\t\t\t"description": ["CAUTION: for Sersic table, fine table",\n'
    linha=linha+'\t\t\t\t\t"(smaller maximum size) should be first.",\n\t\t\t\t\t"Otherwise, you will use coarse resolution",\n'
    linha=linha+'\t\t\t\t\t"table for generating model. Values should",\n\t\t\t\t\t"not be changed with caution."],\n'
    linha=linha+'\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n'
    
    linha=linha+'\t\t\t    "desc": "Model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube cache file",\n\t\t\t    "value": "../../../p800n240r1n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": "800"\n\t\t\t},'
    linha=linha+'\n\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 240\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 1.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'
    linha=linha+'\t\t    "2":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p2000n120r10n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 2000\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 10.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    linha=linha+'\t\t    },\n'

    linha=linha+'\t\t    "3":\t\t\n\t\t    {\n\t\t\t"model":\n'
    linha=linha+'\t\t\t{\n\t\t\t    "desc": "Flux model type",\n\t\t\t    "value": "SERSIC"\n\t\t\t},\n'
    linha=linha+'\t\t\t"cache":\n\t\t\t{\n\t\t\t    "desc": "Image cube name and path",\n\t\t\t    "value": "../../../p1500n120r100n.cache"\n\t\t\t},\n'
    linha=linha+'\t\t\t"size":\n\t\t\t{\n\t\t\t    "desc": "Number of pixels per side",\n\t\t\t    "value": 1500\n\t\t\t},\n'
    linha=linha+'\t\t\t"slices":\n\t\t\t{\n\t\t\t    "desc": "Number of shape slices",\n\t\t\t    "value": 120\n\t\t\t},\n'
    linha=linha+'\t\t\t"rscale":\n\t\t\t{\n\t\t\t    "desc": "Extent of table in radial scale",\n\t\t\t    "value": 100.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmin":\n\t\t\t{\n\t\t\t    "desc": "Minimum value of shape parameter",\n\t\t\t    "value": 0.5\n\t\t\t},\n'
    linha=linha+'\t\t\t"nmax":\n\t\t\t{\n\t\t\t    "desc": "Maximum value of shape parameter",\n\t\t\t    "value": 14.0\n\t\t\t},\n'
    linha=linha+'\t\t\t"central":\n\t\t\t{\n\t\t\t    "desc": "Minimum radius for grid evaluation",\n\t\t\t    "value": 0.1\n\t\t\t}\n'
    
    linha=linha+'\t\t    }\n\t\t}\n\t    },'
  
    linhaPS=linhaNormal+linha+'\n\t    "Point":\n\t    {\n'
    linhaPS=linhaPS+'\t\t"X center":\n\t\t{\n\t\t    "desc": "X center rel to lower corner",\n'
    linhaPS=linhaPS+'\t\t    "index": 0,\n\t\t    "ctrl": "'+galaxy.priorCtrlX+'",\n\t\t    "value": '+galaxy.priorInitX+'\n\t\t},\n'
    linhaPS=linhaPS+'\t\t"Y center":\n\t\t{\n\t\t    "desc": "Y center rel to lower corner",\n'
    linhaPS=linhaPS+'\t\t    "index": 1,\n\t\t    "ctrl": "'+galaxy.priorCtrlY+'",\n\t\t    "value": '+galaxy.priorInitY+'\n\t\t},\n'
    linhaPS=linhaPS+'\t\t"PS mag":\n\t\t{\n\t\t    "desc": "point-source magnitude",\n'
    linhaPS=linhaPS+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlPS_MAG+'",\n\t\t    "value": '+galaxy.priorInitPS_MAG+'\n\t\t}\n\t    },\n'
    linhaPS=linhaPS+'\t    "Sky":\n\t    {\n'
    linhaPS=linhaPS+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linhaPS=linhaPS+'\t\t    "index": 8,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t}\n\t    }\n\t}\n    }\n}'

    linhaNormal=linhaNormal+linha+'\n\t    "Sky":\n\t    {\n'
    linhaNormal=linhaNormal+'\t\t"Sky value":\n\t\t{\n\t\t    "desc": "sky value in ADU counts relative to S",\n'
    linhaNormal=linhaNormal+'\t\t    "index": 7,\n\t\t    "ctrl": "'+galaxy.priorCtrlSky+'",\n\t\t    "value": '+galaxy.priorInitSky+'\n\t\t}\n\t    }\n\t}\n    }\n}'

    filePtr=open(outDir+filename,'w')
    filePtr.write(linhaNormal)
    filePtr.close()

    filePtr=open(outDir+filename+'PS','w')
    filePtr.write(linhaPS)
    filePtr.close()
    
    filePtr=open(outDir+filename+'BMD','w')
    filePtr.write(linhaBMD)
    filePtr.close()    

def scriptGalphat(outDir,galaxy,inputScriptspath,filename,outfilename):
    inputfile=inputScriptspath

    linha='# Run GALPHAT on the Galaxy '+str(galaxy.objid)+' band'+str(galaxy.band)+'\n'
    linha=linha+'# Set a Persistence new session\n'
    linhaReRun='\tprestore new_prior 1\n'

    linha=linha+'\tpnewsession galphat_persist\n\tcktoggle\n'
    linha=linha+'#Simulation setup\n'
    
    linhaNormal=linha+'\tset ndim = 8\n\tset L = 1\n\tset minmc = 6\n'
    linhaPS=linha+'\tset ndim = 9\n\tset L = 1\n\tset minmc = 6\n'
    linhaBMD=linha+'\tset ndim = 12\n\tset L = 1\n\tset minmc = 10\n\tset maxT  = 72\n\tset mchains = 72\n'
    
    linhaNormal=linhaNormal+'\tset nsteps = 50000\n\tset nburn = 20000\n\tset width = 0.1\n\tset maxT = 20.0\n'
    linhaPS=linhaPS+'\tset nsteps = 50000\n\tset nburn = 20000\n\tset width = 0.1\n\tset maxT = 20.0\n'
    linhaBMD=linhaBMD+'\tset nsteps = 65000\n\tset nburn = 20000\n\tset width = 0.1\n\tset maxT = 72.0\n'

    linha='# Setup GALPHAT prior\n'
    linha=linha+'\tset si = new StateInfo(ndim)\n'
    linha=linha+'\tset pvec  = new clivectordist(ndim)\n'
    linha=linha+'\tset X = new '+galaxy.priorX+'\n'
    linha=linha+'\tset Y = new '+galaxy.priorY+'\n'
    linha=linha+'\tset MAG = new '+galaxy.priorMag+'\n'
    linhaBMD=linhaBMD+linha
    linha=linha+'\tset Re = new '+galaxy.priorRe+'\n'
    linha=linha+'\tset N = new '+galaxy.priorN+'\n'
    linha=linha+'\tset Q = new '+galaxy.priorQ+'\n'
    linha=linha+'\tset PA = new '+galaxy.priorPA+'\n'
    linhaNormal=linhaNormal+linha+'\tset Sky= new '+galaxy.priorSky+'\n'
    linhaPS=linhaPS+linha+'\tset PS_MAG= new '+galaxy.priorPS_MAG+'\n'
    linhaPS=linhaPS+'\tset Sky= new '+galaxy.priorSky+'\n'

    linha='\tset BT = new '+galaxy.priorBT+'\n'
    linha=linha+'\tset Rhalf = new '+galaxy.priorRe+'\n'
    linha=linha+'\tset DB = new '+galaxy.priorDB+'\n'
    linha=linha+'\tset N = new '+galaxy.priorN+'\n'
    linha=linha+'\tset Q_disc = new '+galaxy.priorQd+'\n'
    linha=linha+'\tset PA_disc = new '+galaxy.priorPAd+'\n'    
    linha=linha+'\tset Q_bulge = new '+galaxy.priorQb+'\n'
    linha=linha+'\tset PA_bulge = new '+galaxy.priorPAb+'\n'
    linha=linha+'\tset Sky= new '+galaxy.priorSky+'\n'
    linhaBMD=linhaBMD+linha

    linha='#\tX centroid\n\t\tpvec->setval(0, X)\n'
    linha=linha+'#\tY centroid\n\t\tpvec->setval(1, Y)\n'
    linha=linha+'#\tmag\n\t\tpvec->setval(2, MAG)\n'
    linhaBMD=linhaBMD+linha
    linha=linha+'#\trhalf\n\t\tpvec->setval(3, Re)\n'
    linha=linha+'#\tSersic n\n\t\tpvec->setval(4, N)\n'
    linha=linha+'#\taxis ratio\n\t\tpvec->setval(5, Q)\n'
    linha=linha+'#\tposition angle\n\t\tpvec->setval(6, PA)\n'
    linhaNormal=linhaNormal+linha+'#\tsky background ratio\n\t\tpvec->setval(7, Sky)\n'

    linhaPS=linhaPS+linha+'#\tps magnitude\n\t\tpvec->setval(7, PS_MAG)\n'
    linhaPS=linhaPS+'#\tsky background ratio\n\t\tpvec->setval(8, Sky)\n'
 
    linha='#\tB/T\n\t\tpvec->setval(3, BT)\n'
    linha=linha+'#\trhalf\n\t\tpvec->setval(4, Rhalf)\n'
    linha=linha+'#\trd/rb\n\t\tpvec->setval(5, DB)\n'
    linha=linha+'#\tSersic n\n\t\tpvec->setval(6, N)\n'
    linha=linha+'#\tbulge axis ratio\n\t\tpvec->setval(7, Q_bulge)\n'
    linha=linha+'#\tdisc axis ratio\n\t\tpvec->setval(8, Q_disc)\n'
    linha=linha+'#\tbulge position angle\n\t\tpvec->setval(9, PA_bulge)\n'
    linha=linha+'#\tdisc position angle\n\t\tpvec->setval(10, PA_disc)\n'
    linhaBMD=linhaBMD+linha+'#\tsky background ratio\n\t\tpvec->setval(11, Sky)\n'
   
    linhaReRun+=linhaNormal+'\n\tset oldprior = new Prior(si,pvec)\n'
    linhaReRun+='set prior = new PostPrior(oldprior, sstat)'
   
    linha='\n\tset prior = new Prior(si,pvec)\n\tset nsstat = new EnsembleDisc(si)\n'
    linhaBMD+=linha
    linhaNormal+=linha
    linhaPS+=linha
    linhaReRun+='\n\tset nsstat = new EnsembleDisc(si)\n'
    linha='# Convergence test\n'
    linha=linha+'\tset convrg = new GelmanRubinConverge(0, nsstat, "0")\n\tconvrg->setAlpha(0.05)\n\tconvrg->setNgood(100000)\n\tconvrg->setNskip(1000)\n'
    linha=linha+'\tconvrg->setNoutlier(1000)\n\tconvrg->setPoffset(-30.0)\n\tconvrg->setMaxout(12)\n\tconvrg->setRhatMax(1.3)\n'
    linhaBMD=linhaBMD+linha    
    
    linha=linha+'# Metropolis-Hastings\n'
    linha=linha+'\tset mca = new MetropolisHastings()\n\tset mvec  = new clivectord(ndim)\n'
    linha=linha+'\tmvec->setval(0,0.01)\n\tmvec->setval(1,0.01)\n\tmvec->setval(2,0.01)\n'
    linha=linha+'\tmvec->setval(3,0.01)\n\tmvec->setval(4,0.05)\n\tmvec->setval(5,0.01)\n'
    linhaNormal=linhaNormal+linha+'\tmvec->setval(6,0.01)\n\tmvec->setval(7,0.0005)\n\tset mhwidth = new MHWidthOne(si, mvec)\n'
    linhaReRun+=linha+'\tmvec->setval(6,0.01)\n\tmvec->setval(7,0.0005)\n\tset mhwidth = new MHWidthOne(si, mvec)\n'
   
    linhaPS=linhaPS+linha+'\tmvec->setval(6,0.01)\n\tmvec->setval(7,0.01)\n\tmvec->setval(8,0.0005)\n\tset mhwidth = new MHWidthOne(si, mvec)\n'
    
    linha='# DE randomization\n'
    linhaNormal=linhaNormal+linha+'\tset uc = new UniformDist(0.01)\n\tset uc2 = new UniformDist(0.0005)\n\tset eps = new clivectordist(ndim, uc)\n\teps->setval(7, uc2)\n'
    linhaReRun+=linha+'\tset uc = new UniformDist(0.01)\n\tset uc2 = new UniformDist(0.0005)\n\tset eps = new clivectordist(ndim, uc)\n\teps->setval(7, uc2)\n'

    linhaPS=linhaPS+linha+'\tset uc = new UniformDist(0.01)\n\tset uc2 = new UniformDist(0.0005)\n\tset eps = new clivectordist(ndim, uc)\n\teps->setval(8, uc2)\n'
    linhaBMD=linhaBMD+linha+'\tset uc1 = new CauchyDist(0.01)\n\tset uc2 = new UniformDist(0.0005)\n\tset eps = new clivectordist(ndim, uc1)\n\teps->setval(11, uc2)\n'
        
    linha='# Differential Evolution setup\n'
    linha=linha+'\tset mca  = new MetropolisHastings()\n'
    linha=linha+'\tset like = new LikelihoodComputationSerial()\n'
    linha=linha+'\tset sim = new TemperedDifferentialEvolution(si, mchains,minmc,maxT, eps, convrg, prior, like, mca)\n'
    linha=linha+'\tsim->SetLinearMapping(1)\n\tsim->SetJumpFreq(2)i\n\tsim->SetControl(1)\n\tsim->NewGamma(0,03)\n\tsim->EnableLogging()\n'
    linha=linha+'# GALPHAT setup\n'
    linha=linha+'\tset fname = "'+inputfile+'BMD'+'"\n\tset pmap = new GalphatParam(fname)\n\tset outfile = "'+outfilename+'"\n'

    linhaBMD=linhaBMD+linha

    linha='# Differential Evolution setup\n'
    linha=linha+'\tset like = new LikelihoodComputationSerial()\n\tset mchains = 20\n'
    linha=linha+'\tset sim = new DifferentialEvolution(si, mchains, eps, convrg, prior, like, mca)\n'
    linha=linha+'\tsim->SetLinearMapping(1)\n\tsim->SetJumpFreq(2)\n\tsim->SetControl(1)\n\tsim->NewGamma(0.03)\n\tsim->EnableLogging()\n'
    linha=linha+'# GALPHAT setup\n'
    linhaNormal=linhaNormal+linha+'\tset fname = "'+inputfile+'"\n\tset pmap = new GalphatParam(fname)\n\tset outfile = "'+outfilename+'"\n'
    linhaPS=linhaPS+linha+'\tset fname = "'+inputfile+'PS'+'"\n\tset pmap = new GalphatParam(fname)\n\tset outfile = "'+outfilename+'"\n'
    linhaReRun+=linha+'\tset fname = "'+inputfile+'"\n\tset pmap = new GalphatParam(fname)\n\tset outfile = "'+outfilename+'"\n'

    linha='# Run GALPHAT\n'
    linha=linha+'\tset fct = new GalphatLikelihoodFunction(pmap, L)\n'
    linha=linha+'\tsim->SetUserLikelihood(fct)\n\tset current_level = 0\n'
    linha=linha+'\tset run = new RunOneSimulation(nsteps, width, nsstat, prior, sim)\n'
    linha=linha+'\trun->Run()\n'

    linhaNormal+=linha
    linhaReRun+=linha
    linhaPS+=linha
    linhaBMD+=linha

    filePtr=open(outDir+filename+'Rerun','w')
    filePtr.write(linhaReRun)
    filePtr.close()

    filePtr=open(outDir+filename,'w')
    filePtr.write(linhaNormal)
    filePtr.close()

    filePtr=open(outDir+filename+'PS','w')
    filePtr.write(linhaPS)
    filePtr.close()
 
    filePtr=open(outDir+filename+'BMD','w')
    filePtr.write(linhaBMD)
    filePtr.close()
       
def catalogValues( outDirPostProc,ScriptFilename,galaxy):
    filePtr=open(outDirPostProc+ScriptFilename+'_'+str(galaxy.objid)+'_band_'+galaxy.band,'w')
    linha='objid,band,petroMag,deVRadpix,n,deVAB,deVPhi,Sky\n'
    linha=linha+str(galaxy.objid)+','+str(galaxy.band)+','+str(galaxy.petroMag) 
    linha=linha+','+str(galaxy.deVRadpix)+','+str(galaxy.n)
    linha=linha+','+str(galaxy.deVAB)+','+str(galaxy.deVPhi)+','+str(galaxy.priorOffSky)+'\n' 
    filePtr.write(linha)
    filePtr.close()        
    
def priorValues( outDirPostProc,ScriptFilename,galaxy,i):
    filePtr=open(outDirPostProc+ScriptFilename+'_'+i+'_'+str(galaxy.objid)+'_band_'+galaxy.band,'w')
    linha=str(galaxy.objid)+','+str(galaxy.band)+'\n'
    linha=linha+'X\t'+galaxy.priorOffX+'\t'+galaxy.priorCtrlX+'\t'+galaxy.priorX+'\t'+galaxy.priorInitX+'\n'
    linha=linha+'Y\t'+galaxy.priorOffY+'\t'+galaxy.priorCtrlY+'\t'+galaxy.priorY+'\t'+galaxy.priorInitY+'\n'
    linha=linha+'mag\t'+galaxy.priorOffMag+'\t'+galaxy.priorCtrlMag+'\t'+galaxy.priorMag+'\t'+galaxy.priorInitMag+'\n'
    linha=linha+'r_half\t'+galaxy.priorOffRe+'\t'+galaxy.priorCtrlRe+'\t'+galaxy.priorRe+'\t'+galaxy.priorInitRe+'\n'
    linha=linha+'Sersic_n\t'+galaxy.priorOffN+'\t'+galaxy.priorCtrlN+'\t'+galaxy.priorN+'\t'+galaxy.priorInitN+'\n'
    linha=linha+'axis_rat\t'+galaxy.priorOffQ+'\t'+galaxy.priorCtrlQ+'\t'+galaxy.priorQ+'\t'+galaxy.priorInitQ+'\n'
    linha=linha+'pos_ang\t'+galaxy.priorOffPA+'\t'+galaxy.priorCtrlPA+'\t'+galaxy.priorPA+'\t'+galaxy.priorInitPA+'\n'
    linha=linha+'sky\t'+galaxy.priorOffSky+'\t'+galaxy.priorCtrlSky+'\t'+galaxy.priorSky+'\t'+galaxy.priorInitSky+'\n'
    filePtr.write(linha)
    filePtr.close()        
