from math import sin, cos, pi, floor, log10, sqrt
import numpy as np
from astropy.io import fits as pyfits
import copy
import os
import dill

def sourceSize(llk):
    size = np.zeros(2)

    if float(llk[4]) > float(llk[3]) and float(llk[4]) > 0:
        aa = 3*float(llk[9])*float(llk[4])
        bb = 3*float(llk[10])*float(llk[4])
    elif float(llk[3]) > 0:
        aa = 3*float(llk[9])*float(llk[3])*3.
        bb = 3*float(llk[10])*float(llk[3])*3.
    else:
        aa = 3.0
        bb = 3.0
    size[0] = aa
    size[1] = bb

    return size

def modifySexCfgFiles(mainDir, tempDir, back_size, nossoId, time):
    with open(mainDir+'/input2/base_default_model.sex', 'r') as filePtr:
        numeroLinhas = len(filePtr.readlines())

    inp2 = open(mainDir+'input2/base_default_model.sex', 'r')
    out1 = open(tempDir+'base_default.sex', 'w')
    for j in range(0, numeroLinhas):
        ls2 = inp2.readline()
        ll2 = ls2.split()
        if len(ll2) > 0 and ll2[0] == 'CATALOG_NAME':
            ll2[1] = tempDir+'out_sex'+str(nossoId)+'_'+str(time)+'.cat'
            lstrin = ' '
            for k in range(0, len(ll2)):
                lstrin += ll2[k]+' '
            out1.write('%s\n' % lstrin[1:len(lstrin)])
        elif len(ll2) > 0 and ll2[0] == 'BACK_SIZE':
            ll2[1] = back_size
            lstrin = ' '
            for k in range(0, len(ll2)):
                lstrin += ll2[k]+' '
            out1.write('%s\n' % lstrin[1:len(lstrin)])
        else:
            out1.write('%s' % ls2)
    inp2.close()
    out1.close()

def stamp(mainDir, tempDir, outDir, Frameimgfile, tsffile, galaxy, StamSizeRe, EXPTIME, Xpsf, Ypsf, nossoId):
    header, Xc, Yc, StampBigger = generatingBiggerStamp(tempDir, Frameimgfile, galaxy, StamSizeRe)

    print('Modifing Sex Conf. Files')
    # parameter for removing big objects
    BACK_SIZE = '100'

    modifySexCfgFiles(mainDir,tempDir, BACK_SIZE, nossoId, 1)
    
    print('Running Sex - removing big objects')
    os.system('SEXBIN '+tempDir+galaxy.imgfile[:-5]+'_Bigger.fits' +
              ' -c '+tempDir+'base_default.sex'+" >/dev/null 2> /dev/null")

    PA = paCacl(header, galaxy)
 
    gain, readoutNoise, zeroPointVar = zeroPoint(tempDir, tsffile, galaxy, header, EXPTIME)

    infx, infy, supx, supy = auxVars(Xc, StamSizeRe, galaxy, Yc)

    sizeeeX,sizeeeY,sizeeeXs,sizeeeYs = sizeCalc(StamSizeRe, galaxy)
    
    infy, supy, infx, supx, sizeee = auxVarsUpdate(infx, Xc, galaxy, infy, Yc, supx, StampBigger, supy, sizeeeX, sizeeeY, sizeeeXs, sizeeeYs)

    SizeY, SizeX, FinalStamp = finalStamp(StampBigger, infy, supy, infx, supx, Xc, Yc, sizeee, galaxy, outDir, header)

    # library funcion
    headerPsf = pyfits.getheader(outDir+galaxy.psffile, 0)

    numeroLinhas, min_distance, pos_min, mask, mask_source, coeffwhm, mask_center = maskCreation(mainDir, headerPsf, SizeY, SizeX, tempDir, nossoId)
    
    filePtr = open(tempDir+'out_sex'+nossoId+'_1.cat', 'r')

    pos_min, llk, min_distance = findingSecondaryObjects(numeroLinhas, filePtr, Xc, Yc, min_distance, pos_min)

    filePtr = open(tempDir+'out_sex'+nossoId+'_1.cat', 'r')

    aa, bb, phi, theta = maskFiller(numeroLinhas, filePtr, infx, infy, pos_min, mask, llk, galaxy, mask_source, coeffwhm, mask_center)

    filePtr.close()

    pyfits.writeto(tempDir+galaxy.maskfile[:-5]+'_center.fits', mask_center, overwrite=True)
    pyfits.writeto(tempDir+galaxy.maskfile[:-5]+'_source.fits', mask_source, overwrite=True)

    # parameter for removing small objects
    BACK_SIZE = str(10.)
    modifySexCfgFiles(mainDir, tempDir, BACK_SIZE, nossoId, 2)
    print('Running Sex - removing small objects')
    os.system('SEXBIN '+outDir+galaxy.imgfile+' -c '+tempDir +
              'base_default.sex'+" >/dev/null 2> /dev/null")
    
    radius = addSmallGrainToMask(tempDir, nossoId, galaxy, min_distance, coeffwhm, SizeX, SizeY, mask_source, aa, bb, phi, theta, mask)
    if (galaxy.erro == 'Borda'):
        print('Borda')
    else:
        if ((sum(sum(mask_source*mask))) > 0):
            galaxy.erro = 'Source'
        if ((sum(sum(mask_center*mask))) > 0):
            galaxy.erro = 'Center'
    mask_source = np.logical_or(mask_source, mask)*1.0
    mask_center = np.logical_or(mask_center, mask)*1.0

    pyfits.writeto(outDir+galaxy.maskfile, mask, overwrite=True)
    pyfits.writeto(outDir+galaxy.maskfile[:-5]+'_center.fits', mask_center, overwrite=True)
    pyfits.writeto(outDir+galaxy.maskfile[:-5]+'_source.fits', mask_source, overwrite=True)

    headerTest = writeHeader(header, galaxy, SizeX, SizeY, Xpsf, Ypsf, outDir, FinalStamp)

    return galaxy

def maskCreation(mainDir, headerPsf, SizeY, SizeX, tempDir, nossoId):
    coeffwhm = float(headerPsf['coeffwhm'])
    mask = np.zeros((SizeY, SizeX))
    mask_center = np.zeros((SizeY, SizeX))
    mask_source = np.zeros((SizeY, SizeX))
    #  preliminary mask from the stamp creation
    with open(tempDir+'out_sex'+nossoId+'_1.cat', 'r') as filePtr:
        numeroLinhas = len(filePtr.readlines())
    min_distance = 1000
    pos_min = 1
    return numeroLinhas, min_distance, pos_min, mask, mask_source, coeffwhm, mask_center

def paCacl(header, galaxy):
    try:
        SPA = float(header['SPA'])
    except:
        SPA = 0.
        galaxy.erro = 'SPA'
        print("The north possition must be on the image header as SPA")
    PA = -(SPA-galaxy.pa+90.)
    if PA < -90.0:
        PA = 180.0+PA
    if PA > 90.0:
        PA = -(180.0-PA)
    PA = PA*pi
    PA = PA/180.
    PA = str(PA)
    header.append(pyfits.Card('PAoff', PA, 'radians related x+'))
    return PA

def zeroPoint(tempDir, tsffile, galaxy, header, EXPTIME):
    dt = pyfits.open(tempDir+tsffile, ignore_missing_end=True)
    datats = dt[1].data
    dt.close()
    AA = float(datats['aa'][0][int(galaxy.bandCode[galaxy.band]-1)])
    KK = float(datats['kk'][0][int(galaxy.bandCode[galaxy.band]-1)])
    AIRMASS = float(datats['airmass'][0][int(galaxy.bandCode[galaxy.band]-1)])
    dark_variance = float(datats['dark_variance']
                          [0][int(galaxy.bandCode[galaxy.band]-1)])
    gain = float(datats['gain'][0][int(galaxy.bandCode[galaxy.band]-1)])
    readoutNoise = sqrt(dark_variance)*gain
    header.append(pyfits.Card('gain', gain, ''))
    header.append(pyfits.Card('rdnoise', readoutNoise, 'counts'))

    zeroPoint = (10.**(0.4*(AA+KK*AIRMASS)))
    # new Galphat does not takes Exptime from the fits header
    zeroPoint = -2.5*log10(zeroPoint/EXPTIME)
    header.append(pyfits.Card('zP', float(zeroPoint), 'mag'))
    return gain, readoutNoise, zeroPoint

def auxVarsUpdate(infx, Xc, galaxy, infy, Yc, supx, StampBigger, supy, sizeeeX, sizeeeY, sizeeeXs, sizeeeYs):
    if infx < 0:
        infx = 0
        sizeeeX = int(Xc-infx)
        galaxy.erro = 'Borda'
    if infy < 0:
        infy = 0
        sizeeeY = int(Yc-infy)
        galaxy.erro = 'Borda'
    if supx > StampBigger.shape[1]:
        supx = StampBigger.shape[1]
        sizeeeXs = int(supx-Xc)
        galaxy.erro = 'Borda'
    if supy > StampBigger.shape[0]:
        supy = StampBigger.shape[0]
        sizeeeYs = int(supy-Yc)
        galaxy.erro = 'Borda'
    sizeee = min(sizeeeX, sizeeeY, sizeeeXs, sizeeeYs)
    if(galaxy.erro == 'Borda'):
        infx = int(floor(Xc))-sizeee
        supx = int(floor(Xc))+sizeee
        infy = int(floor(Yc))-sizeee
        supy = int(floor(Yc))+sizeee
    return infy, supy, infx, supx, sizeee

def sizeCalc(StamSizeRe, galaxy):
    sizeeeX = int(floor((StamSizeRe)*galaxy.re_pix))
    sizeeeY = int(floor((StamSizeRe)*galaxy.re_pix))+1
    sizeeeXs = int(floor((StamSizeRe)*galaxy.re_pix))
    sizeeeYs = int(floor((StamSizeRe)*galaxy.re_pix))+1

    return sizeeeX,sizeeeY,sizeeeXs,sizeeeYs

def auxVars(Xc, StamSizeRe, galaxy, Yc):
    infx = int(floor(Xc))-int(floor((StamSizeRe)*galaxy.re_pix))
    supx = int(floor(Xc))+int(floor((StamSizeRe)*galaxy.re_pix))+1
    infy = int(floor(Yc))-int(floor((StamSizeRe)*galaxy.re_pix))
    supy = int(floor(Yc))+int(floor((StamSizeRe)*galaxy.re_pix))+1
    
    return infx, infy, supx, supy

def finalStamp(StampBigger, infy, supy, infx, supx, Xc, Yc, sizeee, galaxy, outDir, header):
    FinalStamp = copy.deepcopy(StampBigger[infy:supy, infx:supx])
    galaxy.xc = Xc-(infx)
    galaxy.yc = Yc-(infy)
    SizeX = supx-infx
    SizeY = supy-infy
    pyfits.writeto(outDir+galaxy.imgfile, FinalStamp,
                   header=header, overwrite=True, output_verify='silentfix')
    
    return SizeY, SizeX, FinalStamp

def generatingBiggerStamp(tempDir, Frameimgfile, galaxy, StamSizeRe):
    Frame = pyfits.getdata(tempDir+Frameimgfile, 0)
    header = pyfits.getheader(tempDir+Frameimgfile, 0)

    Extend = 3*int(floor(galaxy.re_pix))
    infxB = int(floor(galaxy.xc))-int(floor((StamSizeRe)*galaxy.re_pix))-Extend
    supxB = int(floor(galaxy.xc)) + \
        int(floor((StamSizeRe)*galaxy.re_pix))+Extend+1
    infyB = int(floor(galaxy.yc))-int(floor((StamSizeRe)*galaxy.re_pix))-Extend
    supyB = int(floor(galaxy.yc)) + \
        int(floor((StamSizeRe)*galaxy.re_pix))+Extend+1
    if infxB < 0:
        infxB = 0
    if infyB < 0:
        infyB = 0
    if supxB > Frame.shape[1]:
        supxB = Frame.shape[1]
    if supyB > Frame.shape[0]:
        supyB = Frame.shape[0]

    StampBigger = copy.deepcopy(Frame[infyB:supyB, infxB:supxB])

    Xc = galaxy.xc-infxB
    Yc = galaxy.yc-infyB
    pyfits.writeto(tempDir+galaxy.imgfile[:-5]+'_Bigger.fits', StampBigger,
                   header=header, overwrite=True, output_verify='silentfix')
    return header, Xc, Yc, StampBigger

def findingSecondaryObjects(numeroLinhas, filePtr, Xc, Yc, min_distance, pos_min):
    for i in range(0, numeroLinhas):
        lsk = filePtr.readline()
        llk = lsk.split()
        if llk[0] != '#':
            X = float(llk[7])
            Y = float(llk[8])
            distance = (((X-Xc)**2+(Y-Yc)**2)**0.5)
            if(distance < min_distance):
                pos_min = i
                min_distance = distance
    return pos_min, llk, min_distance

def maskFiller(numeroLinhas, filePtr, infx, infy, pos_min, mask, llk, galaxy, mask_source, coeffwhm, mask_center):
    for i in range(0, numeroLinhas):
        lsk = filePtr.readline()
        llk = lsk.split()
        if llk[0] != '#':
            Xbig = float(llk[7])
            Ybig = float(llk[8])
            X = Xbig-infx
            Y = Ybig-infy
            theta = float(llk[11])*pi/180.
            thresh = float(llk[6])
            # distance=((Xbig-Xc)**2+(Ybig-Yc)**2)**0.5
            size = sourceSize(llk)
            aa = size[0]
            bb = size[1]
            if pos_min != i:
                for ii in range(int(X-max(size)), int(X+max(size))+1):
                    for jj in range(int(Y-max(size)), int(Y+max(size))+1):
                        distp = ((jj-Y)**2+(ii-X)**2)**0.5
                        if (ii-X) != 0:
                            phi = np.arctan((jj-Y)/(ii-X))
                        else:
                            phi = pi/2
                        radius = aa*bb/((bb*cos(phi-theta)) **
                                        2+(aa*sin(phi-theta))**2)**0.5
                        if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                            radius = 1.5*(thresh/(pi*aa*bb))**0.5*radius
                            if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                                # mask filler
                                mask[jj-1, ii-1] = 1
            else:
                Flux_ISO = float(float(llk[1]))
                Flux_Err_ISO = float(float(llk[len(llk)-1]))
                galaxy.SNR = float(Flux_ISO/Flux_Err_ISO)
                galaxy.Sky = str(float(llk[len(llk)-2]))
                galaxy.petroRadsex = float(llk[4])*float(llk[9])

                for ii in range(int(X-max(aa, bb)), int(X+max(aa, bb))+1):
                    for jj in range(int(Y-max(aa, bb)), int(Y+max(aa, bb))+1):
                        distp = ((jj-Y)**2+(ii-X)**2)**0.5
                        if (ii-X) != 0:
                            phi = np.arctan((jj-Y)/(ii-X))
                        else:
                            phi = pi/2.
                        radius = aa*bb/((bb*cos(phi-theta)) **
                                        2+(aa*sin(phi-theta))**2)**0.5
                        if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                            radius = 1.5*(thresh/(pi*aa*bb))**0.5*radius
                            if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                                # mask filler
                                mask_source[jj-1, ii-1] = 1
                aa = coeffwhm
                bb = aa
                for ii in range(int(X-max(aa, bb)), int(X+max(aa, bb))+1):
                    for jj in range(int(Y-max(aa, bb)), int(Y+max(aa, bb))+1):
                        distp = ((jj-Y)**2+(ii-X)**2)**0.5
                        radius = aa
                        if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                            # mask filler
                            mask_center[jj-1, ii-1] = 1
    return aa, bb, phi, theta

def writeHeader(header, galaxy, SizeX, SizeY, Xpsf, Ypsf, outDir, FinalStamp):
    header.append(pyfits.Card('SkyOff', str(galaxy.Sky), 'count'))
    header.append(pyfits.Card('SNR_F', str(galaxy.SNR), 'count'))
    header.append(pyfits.Card('SIZEX', SizeX, 'pixels'))
    header.append(pyfits.Card('SIZEY', SizeY, 'pixels'))
    header.append(pyfits.Card('XC', galaxy.xc, 'pixels'))
    header.append(pyfits.Card('YC', galaxy.yc, 'pixels'))
    header.append(pyfits.Card('Xpsf', Xpsf, 'para readpsf'))
    header.append(pyfits.Card('Ypsf', Ypsf, 'para readpsf'))
    header.append(pyfits.Card('ErrorF', galaxy.erro,
                              'Error on Stamp Generation'))
    pyfits.writeto(outDir+galaxy.imgfile, FinalStamp,
                   header=header, overwrite=True, output_verify='silentfix')
    return header

def addSmallGrainToMask(tempDir, nossoId, galaxy, min_distance, coeffwhm, SizeX, SizeY, mask_source, aa, bb, phi, theta, mask):
    radius = 0
    with open(tempDir+'out_sex'+nossoId+'_2.cat', 'r') as filePtr:
        numeroLinhas = len(filePtr.readlines())
    filePtr = open(tempDir+'out_sex'+nossoId+'_2.cat', 'r')
    for i in range(0, numeroLinhas):
        lsk = filePtr.readline()
        llk = lsk.split()
        if llk[0] != '#':
            X = float(llk[7])
            Y = float(llk[8])
            distance = (((X-galaxy.xc)**2+(Y-galaxy.yc)**2)**0.5)
            if distance > (min_distance+coeffwhm):
                theta = float(llk[11])*pi/180.
                thresh = float(llk[6])
                size = sourceSize(llk)
                aa = size[0]
                bb = size[1]
                Xi = X
                Yi = Y
                if(Xi >= SizeX):
                    Xi = SizeX-1
                if(Yi >= SizeY):
                    Yi = SizeY-1
                if(Xi < 1):
                    Xi = 1
                if(Yi < 0):
                    Yi = 1
                if ((aa < 2*galaxy.re_pix and bb < 2*galaxy.re_pix)or(mask_source[int(Yi), int(Xi)] == 0)):
                    for ii in range(int(X-max(aa, bb)), int(X+max(aa, bb))+1):
                        for jj in range(int(Y-max(aa, bb)), int(Y+max(aa, bb))+1):
                            distp = ((jj-Y)**2+(ii-X)**2)**0.5
                            if (ii-X) != 0:
                                phi = np.arctan((jj-Y)/(ii-X))
                            else:
                                phi = pi/2.
                            radius = aa*bb/((bb*cos(phi-theta))
                                            ** 2+(aa*sin(phi-theta))**2)**0.5
                            if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                                radius = 1.5*(thresh/(pi*aa*bb))**0.5*radius
                                if radius >= distp and jj-1 >= 0 and ii-1 >= 0 and jj-1 < mask.shape[0] and ii-1 < mask.shape[1]:
                                    # mask filling
                                    mask[jj-1, ii-1] = 1                
    filePtr.close()
    return radius