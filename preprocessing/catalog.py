from astLib import astWCS
from math import log10
from urllib.request import urlopen, URLError
from sys import argv
import astropy.io.fits as pyfits
import os.path
import cutPsf
import generateImages
import generateScript
import pandas as pd
import dill

def download(filename, url):
    if(not(os.path.isfile(filename))):
        attempts = 0
        while attempts < 5:
            try:
                response = urlopen(url, timeout=30)
                content = response.read()
                f = open(filename, 'wb')
                f.write(content)
                f.close()
                break
            except URLError as e:
                attempts += 1


def assingTuplesNames(data, names):
    length = len(names)
    for i in range(0, length):
        globals()[names[i]] = data[i]


class galaxyObj:
    bandCode = {'u': 1, 'g': 2, 'r': 3, 'i': 4, 'z': 5}
    
    def computeGain(self):
        if int(self.run) < 1100:
            gainvec = [[1.62, 3.32, 4.71, 5.165, 4.745], [1.595, 3.855, 4.6, 6.565, 5.155], [1.59, 3.845, 4.72, 4.86, 4.885], [1.6, 3.995, 4.76, 4.885, 4.775], [1.47, 4.05, 4.725, 4.64, 3.48], [2.17, 4.035, 4.895, 4.76, 4.69]]
        else:
            gainvec = [[1.62, 3.32, 4.71, 5.165, 4.745], [1.825, 3.855, 4.6, 6.565, 5.155], [1.59, 3.845, 4.72, 4.86, 4.885], [1.6, 3.995, 4.76, 4.885, 4.775], [1.47, 4.05, 4.725, 4.64, 3.48], [2.17, 4.035, 4.895, 4.76, 4.69]]
        gain = gainvec[int(self.camcol)-1][self.bandCode[self.band]-1]

        return gain

    def __init__(self, objid, ra, dec, band, n, re, q, pa, petroMag, petroRad, rowc, colc, run, rerun, camcol, field, pixelScale):
        print(objid, ra, dec, band, n, re, q, pa, petroMag, petroRad,rowc, colc, run, rerun, camcol, field, pixelScale)
        self.objid = int(objid)
        self.ra = float(ra)
        self.dec = float(dec)
        self.n = float(n)
        self.run = int(run)
        self.rerun = int(rerun)
        self.camcol = int(camcol)
        self.field = int(field)
        self.Mag = float(petroMag)
        self.petroRadpix = float(petroRad)/float(pixelScale)
        self.q = float(q)
        self.pa = float(pa)
        self.re = float(re)
        self.pixelScale = float(pixelScale)
        self.re_pix = float(re)/float(pixelScale)
        self.xc = float(colc)
        self.yc = float(rowc)
        self.zeroPoint = 0.0
        self.SizeX = 0.0
        self.SizeY = 0.0
        self.SNR = 0.0
        self.zeroPoint = 0.0
        self.imgfile = 'img'
        self.psffile = 'psf'
        self.maskfile = 'mask'
        self.band = band
        self.gain = 0.0
        self.priorX = ''
        self.priorY = ''
        self.priorMag = ''
        self.priorRe = ''
        self.priorN = ''
        self.priorQ = ''
        self.priorPA = ''
        self.priorSky = ''
        self.priorCtrlX = ''
        self.priorCtrlY = ''
        self.priorCtrlMag = ''
        self.priorCtrlRe = ''
        self.priorCtrlN = ''
        self.priorCtrlQ = ''
        self.priorCtrlPA = ''
        self.priorCtrlSky = ''
        self.priorOffX = ''
        self.priorOffY = ''
        self.priorOffMag = ''
        self.priorOffRe = ''
        self.priorOffN = ''
        self.priorOffQ = ''
        self.priorOffPA = ''
        self.priorOffSky = ''
        self.priorInitX = ''
        self.priorInitY = ''
        self.priorInitMag = ''
        self.priorInitRe = ''
        self.priorInitN = ''
        self.priorInitQ = ''
        self.priorInitPA = ''
        self.priorInitSky = ''
        self.erro = 'ok'
        self.rdnoise = 1
        self.priorPSmag = ''
        self.priorCtrlPSmag = ''
        self.priorInitPSmag = ''
        self.priorBT = ''
        self.priorCtrlBT = ''
        self.priorInitBT = ''
        self.priorDB = ''
        self.priorCtrlDB = ''
        self.priorInitDB = ''
        self.priorQb = ''
        self.priorCtrlQb = ''
        self.priorInitQb = ''
        self.priorQd = ''
        self.priorCtrlQd = ''
        self.priorInitQd = ''
        self.priorPAb = ''
        self.priorCtrlPAb = ''
        self.priorOffPAb = ''  # see stamp creation
        self.priorInitPAb = ''
        self.priorPAd = ''
        self.priorCtrlPAd = ''
        self.priorInitPAd = ''


##############################Settings########################
i = 0
objid, ra, dec, band, n, petroMag, petroRad, deVRad, deVAB, deVPhi, rowc, colc, run, rerun, camcol, field = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
inputFilePath = str(argv[1])
inputFileName = str(argv[2])
outputCatalog = str(argv[3])

filePointer = open(inputFilePath+'input2/'+argv[2], 'r')
outPath = inputFilePath+outputCatalog

pixelScale = 0.396127  # arcsec/pix
EXPTIME = 53.904  # seg
StamSizeRe = 7.5  # 3 times de Vaucouleurs profile efetive Radius
psfOK = True
stampOK = True
scriptsGalphatOK = True
catalogOut = 'band,nossoId,objid,Mag,re_pix,n,q,pa,Sky,imagenSize,zeroPoint,SNR,petroRadpix,petroMag,deVRad,deVAB,deVPhi,type\n'
PlotImages = True
PlotImagesPanel = False
#################################################################

if not os.path.exists(outPath):
    os.makedirs(outPath)
catalog = filePointer.readlines()
filePointer.close()

def openFiles(filePointer):
    fileComplete = open(outPath+'/listComplete', 'w')
    fileComplete.write(catalogOut)
    filePtr = open(outPath+'/listOk', 'w')
    filePtr.write(catalogOut)

    filePtrErr1 = open(outPath+'/listSource', 'w')
    filePtrErr1.write(catalogOut)

    filePtrErr2 = open(outPath+'/listCenter', 'w')
    filePtrErr2.write(catalogOut)

    filePtrErr3 = open(outPath+'/listBorda', 'w')
    filePtrErr3.write(catalogOut)

    filePtrErr4 = open(outPath+'/listHeader', 'w')
    filePtrErr4.write(catalogOut)

    inputDirGalphat = outPath+'/inputGalphat/'
    if not os.path.exists(inputDirGalphat):
        os.makedirs(inputDirGalphat)

    tempDir = inputDirGalphat+'tmp/'
    if not os.path.exists(tempDir):
        os.makedirs(tempDir)
    return inputDirGalphat, tempDir, filePtr, filePtrErr1, filePtrErr2, filePtrErr3, filePtrErr4, fileComplete

inputDirGalphat, tempDir, filePtr, filePtrErr1, filePtrErr2, filePtrErr3, filePtrErr4, fileComplete = openFiles(filePointer)

def generantingGalphatInput():
    print('Generanting Galphat Input ')
    header = pyfits.getheader(outDirFits+galaxy.imgfile, 0)
    galaxy.erro = (header['ERRORF'])
    galaxy.SNR = (header['SNR_F'])
    galaxy.Sky = (header['SkyOff'])
    generateScript.definePriors(outDirFits, galaxy)
    inputfitspath = '../../inputGalphat/fits/'
    filename = 'input.galphat'+nossoId + '_' + \
        str(galaxy.objid)+'_band_'+str(galaxy.band)
    generateScript.inputGalphat(outDirBand, galaxy, inputfitspath, filename)

    print('Generanting Galphat Script ')
    inputScriptspath = '../../inputGalphat/GalphatScripts/'+filename
    filename = 'script.galphat'+nossoId + '_' + \
        str(galaxy.objid)+'_band_'+str(galaxy.band)
    generateScript.scriptGalphat(outDirBand, galaxy, inputScriptspath, filename, outfilename)

def psfFileCreator(galaxy):
    psfurl = 'http://das.sdss.org/imaging/%d/%d/objcs/%d/psField-%06d-%d-%04d.fit' % \
             (galaxy.run, galaxy.rerun, galaxy.camcol,
              galaxy.run, galaxy.camcol, galaxy.field)
    psffile = 'psField-%06d-%d-%04d.fit' % (
        galaxy.run, galaxy.camcol, galaxy.field)
    if not os.path.exists(tempDir+psffile):
        print('Downloading Psf')
        download(tempDir+psffile, psfurl)
    return psffile

def tsfFileCreator(galaxy):
    tsfurl = 'http://das.sdss.org/imaging/%d/%d/calibChunks/%d/tsField-%06d-%d-%d-%04d.fit' % \
             (galaxy.run, galaxy.rerun, galaxy.camcol, galaxy.run,
              galaxy.camcol, galaxy.rerun, galaxy.field)

    tsffile = 'tsField-%06d-%d-%d-%04d.fit' % (
        galaxy.run, galaxy.camcol, galaxy.rerun, galaxy.field)
    if not os.path.exists(tempDir+tsffile):
        print('Donwloading TsField')
        download(tempDir+tsffile, tsfurl)
    return tsffile

for line in catalog:
    i = i+1
    print(i)
    nossoId = str(i-1)
    line = line.rstrip('\n')
    if(i == 1):
        names = line.split(',')
        continue
    else:
        data = line.split(',')
        ra_sdss = 0
        dec_sdss = 0
        band_sdss = 0
        assingTuplesNames(data, names)
    ra = ra_sdss
    dec = dec_sdss
    band = band_sdss

    if(objid == 0 or ra == 0 or dec == 0 or n == 0 or petroMag == 0 or petroRad == 0 or deVRad == 0 or deVAB == 0 or deVPhi == 0 or rowc == 0 or colc == 0 or run == 0 or rerun == 0 or camcol == 0 or field == 0 or pixelScale == 0 or band == 0):
        print('Catalog must have objid,band,ra,dec,n,petroMag,petroMagErr,deVRad,petroRad,deVPhi,deVAB_u,rowc,colc,run,rerun,camcol,field')
        print(objid, band, ra, dec, n, petroMag, deVRad, petroRad,
              deVPhi, deVAB, rowc, colc, run, rerun, camcol, field)
        continue

    galaxy = galaxyObj(objid, ra, dec, band, n, deVRad, deVAB, deVPhi,
                       petroMag, petroRad, rowc, colc, run, rerun, camcol, field, pixelScale)

    outDirFits = inputDirGalphat+'fits/'
    if not os.path.exists(outDirFits):
        os.makedirs(outDirFits)
    outDirPng = inputDirGalphat+'png/'
    if not os.path.exists(outDirPng):
        os.makedirs(outDirPng)
    outDirPngPanel = inputDirGalphat+'pngPanel/'
    if not os.path.exists(outDirPngPanel):
        os.makedirs(outDirPngPanel)
    
    galaxy.psffile = 'psf'+nossoId + '_' + \
        str(objid) + '_band_' + str(band)+'.fits'
    galaxy.imgfile = 'img'+nossoId + '_' + \
        str(objid) + '_band_' + str(band)+'.fits'
    galaxy.maskfile = 'mask'+nossoId + '_' + \
        str(objid) + '_band_' + str(band)+'.fits'

    if (not os.path.exists(outDirFits+galaxy.psffile)):
        psfOK = True
        print('recreated Psf')
    if (not os.path.exists(outDirFits+galaxy.imgfile) or not os.path.exists(outDirFits+galaxy.maskfile)):
        stampOK = True
        print('recreating stamp and mask')
    if psfOK:
        psffile = psfFileCreator(galaxy)
    imgfile = 'fpC-%06d-%c%d-%04d.fit' % (galaxy.run,
                                          band, galaxy.camcol, galaxy.field)

    if stampOK:
        tsffile = tsfFileCreator(galaxy)

        imgurl = 'http://das.sdss.org/imaging/%d/%d/corr/%d/fpC-%06d-%c%d-%04d.fit.gz' % (
            galaxy.run, galaxy.rerun, galaxy.camcol, galaxy.run, band, galaxy.camcol, galaxy.field)
        if(not(os.path.isfile(tempDir+imgfile))):
            print('Downloading Image')
            download(tempDir+imgfile+'.gz', imgurl)
            os.system('gunzip '+tempDir+imgfile+'.gz')
            if((os.path.isfile(tempDir+imgfile+'.gz'))):
                os.system('rm '+tempDir+imgfile+'.gz')
                pass
        else:
            print('The file '+tempDir+imgfile+' was already donwloaded')

        wcs = astWCS.WCS(tempDir + imgfile)
        Xpsf, Ypsf = wcs.wcs2pix(float(ra), float(dec))
    else:
        Header = pyfits.getheader(
            outDirFits+galaxy.imgfile, ignore_missing_end=True)
        Xpsf = float(Header['Xpsf'])
        Ypsf = float(Header['Ypsf'])

    if psfOK:
        print('Extracting psf')
        if not os.path.isfile(inputFilePath+'read_PSF'):
            print ('\n\n you NEED read_PSF executable from SDSS. Check\n\n \
            http://www.sdss.org/dr7/products/images/read_psf.html\n\n')
            exit()
        os.system(inputFilePath+'read_PSF %s  %i  %f %f  %s' % (tempDir+psffile,
                                                    galaxy.bandCode[band],  Xpsf, Ypsf, tempDir+galaxy.psffile))
        print('Cutting psf')
        cutPsf.run(tempDir+galaxy.psffile, outDirFits+galaxy.psffile)

    if stampOK:
        galaxy = generateImages.stamp(
        inputFilePath, tempDir, outDirFits, imgfile, tsffile, galaxy, StamSizeRe, EXPTIME, Xpsf, Ypsf, nossoId)
    if(PlotImages):
        Path = os.getcwd()
        os.system('Rscript --no-restore --no-save plotImages.R --args '+Path+'/'+outDirPng +
                  ' '+band+' '+nossoId+' '+objid+' '+galaxy.erro+' >/dev/null  2> /dev/null &')

    outDirBand = inputDirGalphat+'/GalphatScripts/'
    if not os.path.exists(outDirBand):
        os.makedirs(outDirBand)

    outfilename = 'posterior'+nossoId+'_' + \
        str(galaxy.objid)+'_band_'+str(galaxy.band)

    if scriptsGalphatOK:
        generantingGalphatInput()

    catalogOut = str(galaxy.band)+','+nossoId+',' + \
        str(galaxy.objid)+','+str(galaxy.Mag)+','+str(galaxy.re_pix)
    catalogOut = catalogOut+',' + \
        str(galaxy.n)+','+str(galaxy.q)+','+str(galaxy.pa)+','+str(galaxy.Sky)
    catalogOut = catalogOut+','+str(galaxy.SizeX)+','+str(galaxy.zeroPoint-2.5*log10(1.0/EXPTIME))+','+str(
        galaxy.SNR)+','+str(galaxy.petroRadpix)+','+petroMag+','+deVRad+','+deVAB+','+deVPhi

    catalogOut += ','+str(galaxy.erro)+'\n'
    
    if (galaxy.erro == 'ok'):
        filePtr.write(catalogOut)
    if(galaxy.erro == 'Source'):
        filePtrErr1.write(catalogOut)
    if(galaxy.erro == 'Center'):
        filePtrErr2.write(catalogOut)
    if(galaxy.erro == 'Borda'):
        filePtrErr3.write(catalogOut)
    else:
        filePtrErr4.write(catalogOut)
    
    fileComplete.write(catalogOut)
    fileComplete.flush()
    filePtr.flush()
    filePtrErr1.flush()
    filePtrErr2.flush()
    filePtrErr3.flush()
    filePtrErr4.flush()

filePtr.close()
filePtrErr1.close()
filePtrErr2.close()
filePtrErr3.close()
filePtrErr4.close()
fileComplete.close()
print('Removing temporaryFiles')
os.system ("rm -rf "+tempDir)
