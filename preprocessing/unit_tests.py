import pytest
import query
import sqlcl
import generateImages as gi
import generateScript as gs
import catalog as cat
import cutPsf
import dill
import numpy

# input values
galaxyFile = open('galaxyAux.pkl', 'rb')
galaxyLocal = dill.load(galaxyFile)
XcLocal = 68.12096999999994
YcLocal = 68.52942000000007
StamSizeReLocal = 7.5

# sourceSize
sizeLocal = numpy.array([24.06096, 22.68288])

# auxVars
infxLocal = 18
infyLocal = 18
supxLocal = 119
supyLocal = 119

# sizeCalc
sizeeeXLocal = 50
sizeeeYLocal = 51
sizeeeXsLocal = 50
sizeeeYsLocal = 51

# finalStamp
SizeYLocal = 101
SizeXLocal = 101
stampFile = open('stampAux.pkl', 'rb')
stampLocal = dill.load(stampFile)
stampBiggerFile = open('stampBiggerAux.pkl', 'rb')
stampBiggerLocal = dill.load(stampBiggerFile)
headerFile = open('headerAux.pkl', 'rb')
headerLocal = dill.load(headerFile)
sizeeeLocal = 50
tempDirLocal = 'catalog/inputGalphat/tmp/'
outDirLocal = 'catalog/inputGalphat/fits/'

# unknown
FrameimgfileLocal = 'fpC-004649-r2-0375.fit'

# paCacl
PALocal = '-0.705019834677'

# zeroPoint
gainLocal = 4.599999904632568
readoutNoiseLocal = 4.599999904632568
zeroPointVarLocal = 28.15578103929287

# auxVarsUpdate
infxLocal1 = 18
infyLocal1 = 18
supyLocal1 = 119
supxLocal1 = 119

# magica
numeroLinhasLocal = 19
min_distanceLocalIn = 1000
min_distanceLocalOut = 53.2948436768259
pos_minLocalIn = 1
pos_minLocalOut = 16
filePtrMagica = open('sexTest_2.cat', 'r')
llkLocal = ['5', '28.86792', '-3.6510', '6.91', '9.90', '3', '3',
    '29.6437', '111.3114', '0.577', '0.333', '43.08', '1107.375', '8.710968']

# maskCreation
headerPsfFile = open('headerPsfAux.pkl', 'rb')
headerPsfLocal = dill.load(headerPsfFile)
numeroLinhasMaskLocal = 21


# magica1
aaLocal = 17.1369
bbLocal = 9.890100000000002
phiLocal = 0.7954538391469324
thetaLocal = 0.751887841759157
coeffwhmLocal = 3.361920634072423
maskFile = open('maskAux.pkl', 'rb')
maskLocal = dill.load(maskFile)
mask_centerFile = open('mask_centerAux.pkl', 'rb')
mask_centerLocal = dill.load(mask_centerFile)
mask_sourceFile = open('mask_sourceAux.pkl', 'rb')
mask_sourceLocal = dill.load(mask_sourceFile)
filePtrMagica1 = open('sexTest_1.cat', 'r')

# addSmallGrainToMask
radiusLocal = 44.346958536772675

# writeHeader
headerTestFile = open('headerTestAux.pkl', 'rb')
headerTestLocal = dill.load(headerTestFile)

# stamp
EXPTIMELocal = 53.904
XpsfLocal = 1538.5000366
YpsfLocal = 1023.98764468
nossoIdLocal = '1'
tsffileLocal = 'tsField-004649-2-40-0375.fit'

# definePriors
outDirFitsLocal = 'catalog/inputGalphat/fits/'

# psfFileCreator
psfLocal = 'psField-004649-2-0375.fit'

# tsfFileCreator
tsfLocal = 'tsField-004649-2-40-0375.fit'

# query
queryLocal = ['587739719773127035,240.91361467,17.18485200,r,4,16.10554900,0.00525723,2.68738200,4.64679600,163.98133900,0.66096800,1024.52942000,1539.12097000,4649,40,2,375,587739719773127035']
fileInputLocal = open('input1/input.csv', 'r')
fileOutputLocal = open('input2/input_result.csv', 'w')
fileErrorLocal = open('input2/input_resultError.csv',  'w')
dataLocal = ['objid\n', '587739719773127035']

# buildQuery
queryBuildLocal = 'select P.objid,P.ra,P.dec,P.petroMag_r,P.petroMagErr_r,P.deVRad_r,P.petroRad_r,P.deVPhi_r,P.deVAB_r,P.rowc_r,P.colc_r,P.run,P.rerun,P.camcol,P.field from PhotoObjAll P where P.objid =  587739719773127035'
bandLocal = 'r'
objidLocal = '587739719773127035'

# filtercomment
sqlLocal = 'select P.objid,P.ra,P.dec,P.petroMag_r,P.petroMagErr_r,P.deVRad_r,P.petroRad_r,P.deVPhi_r,P.deVAB_r,P.rowc_r,P.colc_r,P.run,P.rerun,P.camcol,P.field from PhotoObjAll P where P.objid =  587739719773127035'
fsqlFile = open('fsqlAux.pkl', 'rb')
fsqlLocal = dill.load(fsqlFile)

# dofwhm
dofwhmReturnLocal = 3.361920634072423
psfdataFile = open('psfdataAux.pkl', 'rb')
psfdataLocal = dill.load(psfdataFile)

# gauss
x0Local = 16.00675534875658
y0Local = 16.001833118448424
intensLocal = 27260.438194916955
sigmaLocal = 1.4275671482260819


def test_sourceSize():
    size = gi.sourceSize(llkLocal)
    assert size.all() == sizeLocal.all()


def test_unknown():
    header, Xc, Yc, StampBigger = gi.unknown(
        tempDirLocal, FrameimgfileLocal, galaxyLocal, StamSizeReLocal)
    assert header == headerLocal
    assert Xc == XcLocal
    assert Yc == YcLocal
    assert StampBigger.all() == stampBiggerLocal.all()


def test_paCalc():
    PATest = gi.paCacl(headerLocal, galaxyLocal)
    assert round(PATest, 4) == round(PALocal, 4)


def test_zeroPoint():
    gain, readoutNoise, zeroPointVar = gi.zeroPoint(
        tempDirLocal, tsffileLocal, galaxyLocal, headerLocal, EXPTIMELocal)
    assert gain == gainLocal
    assert readoutNoise == readoutNoiseLocal
    assert zeroPointVar == zeroPointVarLocal


def test_auxVars():
    infx, infy, supx, supy = gi.auxVars(
        XcLocal, StamSizeReLocal, galaxyLocal, YcLocal)
    assert infx == infxLocal
    assert infy == infyLocal
    assert supx == supxLocal
    assert supy == supyLocal


def test_sizeCalc():
    sizeeeX, sizeeeY, sizeeeXs, sizeeeYs = gi.sizeCalc(
        StamSizeReLocal, galaxyLocal)
    assert sizeeeX == sizeeeXLocal
    assert sizeeeY == sizeeeYLocal
    assert sizeeeXs == sizeeeXsLocal
    assert sizeeeYs == sizeeeYsLocal


def test_auxVarsUpdate():
    infy, supy, infx, supx, sizeee = gi.auxVarsUpdate(infxLocal, XcLocal, galaxyLocal, infyLocal, YcLocal,
                                                      supxLocal, stampBiggerLocal, supyLocal, sizeeeXLocal, sizeeeYLocal, sizeeeXsLocal, sizeeeYsLocal)
    assert infy == infyLocal1
    assert supy == supyLocal1
    assert infx == infxLocal1
    assert supx == supxLocal1
    assert sizeee == sizeeeLocal


def test_finalStamp():
    SizeY, SizeX, FinalStamp = gi.finalStamp(stampBiggerLocal, infyLocal, supyLocal, infxLocal,
                                             supxLocal, XcLocal, YcLocal, sizeeeLocal, galaxyLocal, outDirLocal, headerLocal)
    assert SizeX == SizeXLocal
    assert SizeY == SizeYLocal
    assert FinalStamp.all() == stampLocal.all()


def test_maskCreation():
    numeroLinhas, min_distance, pos_min, mask, mask_source, coeffwhm, mask_center = gi.maskCreation(
        headerPsfLocal, SizeYLocal, SizeXLocal, tempDirLocal, nossoIdLocal)
    assert numeroLinhas == numeroLinhasMaskLocal
    assert min_distance == min_distanceLocalIn
    assert pos_min == pos_minLocalIn
    assert mask.all() == maskLocal.all()
    assert mask_source.all() == mask_sourceLocal.all()
    assert round(coeffwhm, 4) == round(coeffwhmLocal, 4)
    assert mask_center.all() == mask_centerLocal.all()


def test_magica():
    pos_min, llk, min_distance = gi.magica(
        numeroLinhasLocal, filePtrMagica, XcLocal, YcLocal, min_distanceLocalIn, pos_minLocalIn)
    assert pos_min == pos_minLocalOut
    assert llk == llkLocal
    assert min_distance == min_distanceLocalOut


def test_magica1():
    aa, bb, phi, theta = gi.magica1(numeroLinhasLocal, filePtrMagica1, infxLocal1, infyLocal1,
                                    pos_minLocalOut, maskLocal, llkLocal, galaxyLocal, mask_sourceLocal, coeffwhmLocal, mask_centerLocal)
    assert aa == aaLocal
    assert bb == bbLocal
    assert phi == phiLocal
    assert theta == thetaLocal


def test_addSmallGrainToMask():
    radius = gi.addSmallGrainToMask(tempDirLocal, nossoIdLocal, galaxyLocal, min_distanceLocalOut, coeffwhmLocal,
                                    SizeXLocal, SizeYLocal, mask_sourceLocal, aaLocal, bbLocal, phiLocal, thetaLocal, maskLocal)
    assert round(radius, 4) == round(radiusLocal, 4)

def test_stamp():
    galaxyTest=gi.stamp(tempDirLocal, outDirLocal, FrameimgfileLocal, tsffileLocal,
                        galaxyLocal, StamSizeReLocal, EXPTIMELocal, XpsfLocal, YpsfLocal, nossoIdLocal)
    assert galaxyTest == galaxyLocal

def test_definePriors():
  galaxyTest=gs.definePriors(outDirFitsLocal, galaxyLocal)
  assert galaxyTest == galaxyLocal

def test_psfFileCreator():
    psfTest=cat.psfFileCreator(galaxyLocal)
    assert psfTest == psfLocal

def test_tsfFileCreator():
    tsfTest=cat.tsfFileCreator(galaxyLocal)
    assert tsfTest == tsfLocal

def test_getInput():
    dataTest=query.getInput(fileInputLocal)
    assert dataTest == dataLocal

def test_query():
   queryTest=query.getData(dataLocal, fileOutputLocal, fileErrorLocal)
   assert queryTest == queryLocal

def test_buildQuery():
    queryBuildTest=query.buildQuery(bandLocal, objidLocal)
    assert queryBuildTest == queryBuildLocal

def test_filterComment():
    fsql=sqlcl.filtercomment(sqlLocal)
    assert fsql == fsqlLocal

def test_dofwhm():
    returnTest=cutPsf.dofwhm(psfdataLocal)
    assert round(returnTest, 4) == round(dofwhmReturnLocal, 4)
