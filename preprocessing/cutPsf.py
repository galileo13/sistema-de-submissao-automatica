from astropy.io import fits
import astropy.io.fits as pyfits
import numpy
import os
import scipy.optimize as opt
import dill


def gauss(xy, x0, y0, intens, sigma):
    (x, y) = xy
    return intens*numpy.exp(-(numpy.power(x-x0, 2)+numpy.power(y-y0, 2))/(2.*sigma**2)).ravel()


def dofwhm(psfdata):
    # importing and storing object for tests
    # psfdataFile = open('psfdataAux.pkl', 'wb')
    # dill.dump(psfdata, psfdataFile) #dump object for unit testing
    
    x = numpy.arange(psfdata.shape[1])
    y = numpy.arange(psfdata.shape[0])
    x, y = numpy.meshgrid(x, y)

    popt, pcov = opt.curve_fit(gauss, (x, y), psfdata.ravel(), p0 = [psfdata.shape[1]/2, psfdata.shape[0]/2, psfdata[psfdata.shape[1]//2, psfdata.shape[0]//2], 5.0])
    # plt.figure()

    return 2.355*abs(popt[3])


def run(psfFilename, psfFinalFilename):
    print('Reading '+psfFilename)
    psf = pyfits.getdata(psfFilename, memmap=False)
    Sky = 1000

    header = pyfits.getheader(psfFilename, 0)

    os.system('rm '+psfFilename)

    # hdulist.close()
    n = int(numpy.sqrt(numpy.size(psf)))
    flagxmin = 1
    flagymin = 1
    flagxmax = 1
    flagymax = 1

    for i in range(1, int(n/2)):
        if ((min(psf[i, :]) != Sky or max(psf[i, :]) != Sky) and flagxmin == 1):
            xmin = i-1
            flagxmin = 0
        if ((min(psf[:, i]) != Sky or max(psf[:, i]) != Sky) and flagymin == 1):
            ymin = i-1
            flagymin = 0
        if ((min(psf[n-i, :]) != Sky or max(psf[n-i, :]) != Sky) and flagxmax == 1):
            xmax = n-i
            flagxmax = 0
        if ((min(psf[:, n-i]) != Sky or max(psf[:, n-i]) != Sky) and flagymax == 1):
            ymax = n-i
            flagymax = 0
    nx = (xmax-xmin)
    ny = (ymax-ymin)
    newPsf = numpy.zeros((nx, ny))
    for i in range(0, int(nx-1)):
        for j in range(0, int(ny-1)):
            newPsf[i, j] = (psf[i+xmin, j+ymin]-Sky)
    coeffwhm = dofwhm(newPsf)

    header.append(pyfits.Card('coeffwhm', coeffwhm, 'pixels'))
    pyfits.writeto(psfFinalFilename, newPsf/(sum(sum(newPsf))),
                   header=header, overwrite=True, output_verify='warn')
    print(('Writing '+psfFinalFilename))
