import sqlcl
import string
from sys import argv
import sys
import os

def openFile(file, operation):
    file = open(file, operation)
    return file

def getInput(file):
    data = file.readlines()
    file.close()
    return data

def assingTuplesNames(data, names):
    length = len(names)
    for i in range(0, length):
        if '.' in(str(data[i])):
            dat="{:10.8f}".format(float(data[i]))
        else:
            dat=data[i]
            if '\n' in(str(data[i])): 
                dat=dat[:-1]
        data[i]=dat
        removeBandFromNames(i, names)
        globals()[names[i]] = str(dat)

def removeBandFromNames(i, names):
    if i>=3 and i<=10 :
        name_Band=names[i]
        names[i]=name_Band[:-2]
    if i==14 :
        name_Band=names[i]
        names[i]=name_Band[:-1]
    if i==1 or i==2:
        name_Band=names[i]+'_sdss'
        names[i]=name_Band    

# done
def getData(data, fileOutPointer, fileOutPointerErro):
    i = 0
    headerLine = 0
    ra = 0 # ??
    dec = 0 # ??
    query = []

    #constants
    bandName = 'band_sdss'
    band = 'r'
    comma = ","
    
    for line in data:
        i = i+1
        if line == '\n':
            break
        line = line.rstrip('\n')
        if(i == 1):
            namesIn = line.split(',')
            fileOutPointerErro.write(line)
            continue
        else:
            dataIn = line.split(',')
        length = len(namesIn)
        for j in range(0, length):
            globals()[namesIn[j]] = dataIn[j]
        
        print(objid)
        
        sql_query = buildQuery(band, objid)
        query_result = sqlcl.query(sql_query).readlines()
        
        names = str(query_result[0].decode('utf-8')).split(',')
        
        if len(query_result) > 1:
            query_result = query_result[1]
        else:
            print ("No object found")
            print (ra, dec, band)
            line = objid+'\n'

            fileOutPointerErro.write(line)
            continue
        
        data = str(query_result.decode('utf-8')).split(',')
        headerLine = headerLine+1
        
        assingTuplesNames(data, names)

        addBandInformation(names, bandName, data, band)

        if headerLine == 1:
            header = comma.join(names+namesIn)
            fileOutPointer.write(header+'\n')
        
        line = comma.join(data+dataIn)
        print(line)
        query.append(line)

    return query

def addBandInformation(names, bandName, data, band):
    positionBandName = 3
    positionBand = 4
    
    names.insert(positionBandName, bandName)
    data.insert(positionBandName, band)
    # SDSS model de Vaucouleurs profile
    names.insert(positionBand, 'n')
    data.insert(positionBand, str(4))

# done
def buildQuery(band, objid):
    sql_query = 'select P.objid'
    sql_query = sql_query+',P.ra'
    sql_query = sql_query+',P.dec'
    sql_query = sql_query+',P.petroMag_'+band
    sql_query = sql_query+',P.petroMagErr_'+band
    sql_query = sql_query+',P.deVRad_'+band
    sql_query = sql_query+',P.petroRad_'+band
    sql_query = sql_query+',P.deVPhi_'+band
    sql_query = sql_query+',P.deVAB_'+band
    sql_query = sql_query+',P.rowc_'+band
    sql_query = sql_query+',P.colc_'+band
    sql_query = sql_query+',P.run'+',P.rerun'+',P.camcol'+',P.field'
    sql_query = sql_query+' from PhotoObjAll P'
    sql_query = sql_query+' where P.objid =  '+objid
    return sql_query

def writeOutput(query, file):
    for line in query:
        file.write(line+'\n')

# input info
inputFilePath = str(argv[1])
inputFileName = str(argv[2])

inputFile=inputFilePath+'input1/'+argv[2]
outputFile=inputFilePath+'input2/'+argv[2]
errorFile = inputFilePath+'input2/'+argv[2]+'Error.csv'

fileInput = openFile(inputFile, "r")
fileOutput = openFile(outputFile, "w")
fileError = openFile(errorFile,  "w")

#getting input
input = getInput(fileInput)

#getting data from server
data = getData(input, fileOutput, fileError)

#writing data to output file
writeOutput(data, fileOutput)