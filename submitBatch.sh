#!/bin/csh

#Documentation
# Setup file that defines all necessary folders and files before new batch on SD for Sersic + Exponential model
# Example of running -> csh submitBatch.sh SD 1 9
# first argument is machine where script will be executed
# second argument is number of galaxies per list
# thrid argument is total of galaxie to process.


echo "Reading congig.txt"
source config.txt

# Definition of important path
set mainFolder="preprocessing"
set processingFolder=$catalogName
set processedFolder="../processed"
set preprocessingFolder="preprocessing"
set galaxySublistPath=$PWD"/"$preprocessingFolder"/"

set galaxyListPath=$PWD"/"$preprocessingFolder"/input0/"
#to change an input list, just change this var to its name
set galaxyListName=$inputList
set machine=$machine
echo "Running on "$machine

# calculus of distribution of galaxies
set galaxiesPerList=$galaxiesPerList
set numberGalaxies=$totalGalaxies

set totalTime=0
set model=$modelConfig
if ( $model == "sersic" ) then
    echo "Processing with Sersic model"
    set modelName="Sersic"
    set threadsPerGalaxy=20
    set threadPerNode=24
    set mediumTime=2 #hours
    @ totalTime=($mediumTime * $galaxiesPerList)
else
    echo "Processing with Sersic+Exponential model"
    set modelName="SersicPlusExp"
    set threadsPerGalaxy=72
    set threadPerNode=24
    set mediumTime=10 #hours
    @ totalTime=($mediumTime * $galaxiesPerList)
endif

set numberInstances=0
@ numberInstances=$numberGalaxies / $galaxiesPerList
set numberCores=0
@ numberCores=$numberInstances * $threadsPerGalaxy


# calling python aux file to find the numer of nodes needed for processing.
# reason for this is the fact that csh does not support float calculations
python $mainFolder/nodeCalculator.py $numberCores $threadPerNode 2>&1
set numberNodes=$?
set queueName=$queueName


set slurmFileName=$slurmFileNameConfig
set slurmFilePath=$preprocessingFolder"/scriptsRunAndCheck/"
# 0. Check if there are need to create folders.
if( -d $processedFolder) then
    echo "the folder "$processedFolder" exists"
else
    echo "creating" $mainFolder"/"$processedFolder
    mkdir $processedFolder

endif

# 1. Check if there are processed folders, if there is, copy to processed folder
if( -d $mainFolder"/"$processingFolder"1") then
    echo "the folder "$mainFolder"/"$processingFolder"1" " exists. Remove it before continuing"
    #UNCOMENT FOR PRODUCTION
    exit(1)
else
    # 2. Create new folders for next processing
    echo "creating catalogs from 1 to "$numberInstances
    set aux = $numberInstances
    while ( $aux >= 1 )
        mkdir $mainFolder"/"$processingFolder$aux
        ln -s $PWD/$mainFolder/$processingFolder$aux "../"$processingFolder$aux
        @ aux--
    end
endif

# 3. Split a big list in N list for each instance
python $mainFolder/splitList.py $galaxiesPerList $galaxyListPath $galaxyListName $numberInstances
# 3.1 Moving file to its places
set aux = $numberInstances
mkdir $mainFolder"/input1/"
while ( $aux >= 1 )
    mv $galaxyListPath$aux".csv" $mainFolder"/input1/"
    @ aux--
end

# 4. Run query.py
set aux = $numberInstances
while ( $aux >= 1 )
    #UNCOMENT FOR PRODUCTION
    echo "Query for catalog "$aux
    nohup python $mainFolder/query.py $galaxySublistPath $aux".csv" &
    sleep 30
    #rm $mainFolder"/input1/"$aux".csv"
    @ aux--
end

# 5. Run catalog.py
set aux = $numberInstances
while ( $aux >= 1 )
    # if you got error with out_sex, edit base_default_model, ajusting correct path for default.param and default.conv
    #UNCOMENT FOR PRODUCTION
    nohup python $mainFolder/catalog.py $galaxySublistPath $aux".csv" $processingFolder$aux & #some error, need more testing
    sleep 20
    #rm $mainFolder"/input2/"$aux".csv"
    @ aux--
end
# 6. Construct Slurm file (Santos Dumont) or run submitJobs (Pollux) 
switch ($machine)
	case "sd" :
        # construction of slurmFile
        echo "Generating slurm file"
        python $mainFolder/createSlurmFile.py $slurmFilePath $slurmFileName $numberInstances $numberNodes $numberCores $queueName $totalTime $modelName $machine $PWD $SCRATCH

        # submission of job
        #sbatch $slurmFilePath/$slurmFileName
        breaksw
    case "generic" :
        set aux = $numberInstances
        while ( $aux >= 1 )
            #UNCOMENT FOR PRODUCTION
            echo "nohup csh submitJobsMain $HOME/$processingFolder$aux/listComplete $modelName $machine &"
            @ aux--
        end
        
        breaksw
    default :
        echo "I didn't understand the machine where the processing will be performed."
        echo "The first argument should be: sd|generic"
        
        breaksw
endsw
