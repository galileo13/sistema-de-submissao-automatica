

# installing dependecies
sudo apt update
sudo apt upgrade
sudo apt install -y git mpich gcc g++ make autotools-dev automake libtool nano flex python bison libcommoncpp2-dev netcdf-bin libnetcdff-dev doxygen libnetcdf-dev libreadline6 libreadline6-dev libdb-dev libdb++-dev exuberant-ctags fftw3 fftw3-dev pkg-config gtk+-2.0 libboost-all-dev libgsl-dev libcfitsio-bin libcfitsio-dev

# cloning the source code
git clone https://galileo13@bitbucket.org/mdweinberg/bie.git
cd bie
# edit line 74 in srs/util/MPICommunicationSession.cc from MPI_Type_struct(numFields, len, loc, typ, &dtype); to MPI_Type_create_struct(numFields, len, loc, typ, &dtype);

# starting the building
aclocal -I m4 && autoheader && libtoolize --force && automake --add-missing && autoconf
# boost error fix and place of installation
./configure --with-boost-libdir=/usr/lib/x86_64-linux-gnu/ --prefix=/opt/bie

# make using 6 cores
make -j6 CXXFLAGS="-O3 -fno-inline-small-functions" CFLAGS="-O3"
# installation
sudo make install

# exporting path to be able to invoke the bie executable
export PATH=/opt/bie/bin:${PATH}
export LD_LIBRARY_PATH=/opt/bie/lib/bie:${LD_LIBRARY_PATH}

# testing the setup with 6 cores
cd Galphat/user/
mpirun -np 6 bie -- -mpi -f script.galphat

