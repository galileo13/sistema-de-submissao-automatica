#!/bin/bash

# setting vars
preprocessing=$PWD/preprocessing
echo $preprocessing

# installing read psf
cd deps
unzip readPSF_install.zip
cd readPSF_install
make clean
make
cp read_PSF $preprocessing
cd ../..

# editing base model
paramPath=$preprocessing"/default.param"
convPath=$preprocessing"/default.conv"

sed -i -e "s~PARAMPATH~$paramPath~g" $preprocessing/input2/base_default_model.sex
sed -i -e "s~CONVPATH~$convPath~g" $preprocessing/input2/base_default_model.sex

# setting the correct sex bin path
if [ $# -eq 0 ]; then
    sex -v
    if [ $? -eq 0 ]; then
        echo "No arguments supplied, setting sextractor to sex"
        sed -i -e "s~SEXBIN~sex~g" $preprocessing/generateImages.py
    else
        echo "No arguments supplied, setting sextractor to sextractor"
        sed -i -e "s~SEXBIN~sextractor~g" $preprocessing/generateImages.py
    fi
else
    echo "Argument supplied, setting sextractor to" $1
    sed -i -e "s~SEXBIN~$1~g" $preprocessing/generateImages.py
fi


# setting python environment
python3 -m venv pypygalphat
source ./pypygalphat/bin/activate
pip install -r requirements.txt